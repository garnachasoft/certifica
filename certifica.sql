-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-07-2016 a las 12:15:50
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `certifica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_certificates`
--

CREATE TABLE `im_certificates` (
  `id` int(11) NOT NULL,
  `uid` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rut` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anexo1` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `anexo2` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `anexo3` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `im_certificates`
--

INSERT INTO `im_certificates` (`id`, `uid`, `sku`, `document`, `rut`, `nombre`, `email`, `anexo1`, `anexo2`, `anexo3`, `created_at`, `updated_at`) VALUES
(1, '578488ba30150', '', '5775d58603770', '30.686.957-4', 'Guillermo Canales, Antonia perez, Daniel Chacome', 'memoadian@gmail.com, antonia@mail.com', 'trabajo 1', 'dato1', 'unomas', '2016-07-12 11:05:46', '2016-07-12 11:05:46'),
(2, '578488ba35b5c', '', '5775d58603770', '30.686.957-5', 'Daniel perez chacome', 'danielperez@mail.com', 'trabajo 2', 'dato2', 'unomas', '2016-07-12 11:05:46', '2016-07-12 11:05:46'),
(3, '578488ba36abc', '', '5775d58603770', '30.686.957-6', 'Viridiana Enceres', 'virids@mail.com', 'trabajo 3', 'dato3', 'unomas', '2016-07-12 11:05:46', '2016-07-12 11:05:46'),
(4, '578488ba38a17', '', '5775d58603770', '30.686.957-7', 'Juan Covarrubias', 'famc@mail.com', 'trabajo 4', 'dato4', 'unomas', '2016-07-12 11:05:46', '2016-07-12 11:05:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_documents`
--

CREATE TABLE `im_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `signs` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `im_documents`
--

INSERT INTO `im_documents` (`id`, `uid`, `name`, `description`, `notes`, `signs`, `template`, `created_at`, `updated_at`) VALUES
(1, '576ce7225d7b8', 'Hackatón', 'Por haber hackeado Google', '', '576ccd03cfd4d|576cd1b6841a7', '577822fab897e', '2016-07-02 20:24:26', '2016-07-03 01:24:26'),
(2, '5775d58603770', 'adadssd', 'aasdas', 'asdadsdas', '576ccd03cfd4d|576cd1b6841a7', '5775d6e7b047d', '2016-07-12 01:13:44', '2016-07-12 06:13:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_password_resets`
--

CREATE TABLE `im_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_pictures`
--

CREATE TABLE `im_pictures` (
  `id` int(11) NOT NULL,
  `uid` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `md5` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_type` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `url` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `exif` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `im_pictures`
--

INSERT INTO `im_pictures` (`id`, `uid`, `md5`, `content_type`, `width`, `height`, `url`, `created_at`, `updated_at`, `exif`, `group`) VALUES
(0, 'avatar', '3d50e83f7586325304f88584f699ad33', 'image/png', 147, 147, '3d50e83f7586325304f88584f699ad33.png', '2015-09-09 18:33:15', '2015-09-09 18:33:15', 'null', 'UserProfile'),
(27, '563037535d936', '2018fcb4710b2e4eaf50781af53bf002', 'image/jpeg', 250, 250, '2018fcb4710b2e4eaf50781af53bf002.jpg', '2015-10-28 02:47:47', '2015-10-28 02:47:47', '{"FileName":"2018fcb4710b2e4eaf50781af53bf002.jpg","FileDateTime":1446000467,"FileSize":25981,"FileType":2,"MimeType":"image\\/jpeg","SectionsFound":"","COMPUTED":{"html":"width=\\"250\\" height=\\"250\\"","Height":250,"Width":250,"IsColor":1}}', 'UserProfile'),
(31, '576ccac0b8a10', '9dd5219acb459d491e9ce21066dfd513', 'image/png', 800, 189, '9dd5219acb459d491e9ce21066dfd513.png', '2016-06-24 10:53:05', '2016-06-24 10:53:05', 'null', 'Signs'),
(32, '576cd1a8140c1', '59ff40461ee5d3fae5719aaa7e42ef15', 'image/png', 488, 198, '59ff40461ee5d3fae5719aaa7e42ef15.png', '2016-06-24 11:22:32', '2016-06-24 11:22:32', 'null', 'Signs'),
(33, '5773bcb1b17cf', 'e8693c420641e98f9177dbac38fea4d3', 'image/png', 1280, 905, 'e8693c420641e98f9177dbac38fea4d3.png', '2016-06-29 17:18:59', '2016-06-29 17:18:59', 'null', 'ForTemplate'),
(34, '57743345b5838', '05fe850c77d93b80063db5475dc242db', 'image/png', 839, 551, '05fe850c77d93b80063db5475dc242db.png', '2016-06-30 01:44:54', '2016-06-30 01:44:54', 'null', 'ForTemplate');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_settings`
--

CREATE TABLE `im_settings` (
  `id` int(11) NOT NULL,
  `key` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedBy` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_signs`
--

CREATE TABLE `im_signs` (
  `id` int(11) NOT NULL,
  `uid` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `handwrite` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `im_signs`
--

INSERT INTO `im_signs` (`id`, `uid`, `name`, `title`, `handwrite`, `created_at`, `updated_at`) VALUES
(2, '576ccd03cfd4d', 'Paul McCarney', 'Bass Guitar', '576ccac0b8a10', '2016-06-24 11:02:43', '2016-06-24 11:02:43'),
(3, '576cd1b6841a7', 'Daniel García', 'Hackster', '576cd1a8140c1', '2016-06-24 11:22:46', '2016-06-24 11:22:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_templates`
--

CREATE TABLE `im_templates` (
  `id` int(11) NOT NULL,
  `uid` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `orientation` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `im_templates`
--

INSERT INTO `im_templates` (`id`, `uid`, `name`, `layout`, `orientation`, `created_at`, `updated_at`) VALUES
(5, '5775d6e7b047d', '', '\n						<div class="nocanvas"></div>\n						\n						<div class="nocanvas"></div>\n						\n		<div class="item resizable draggable focus" data-uid="0b81edc5c23558f3" data-type="img" data-width="1280" data-height="905" data-firstsize="nope" style="width: 1107.63px; height: 777.282px; left: 2.36926px; top: 2.32212px; right: auto; bottom: auto;">\n			<div class="anchor" style="left: -6px; top: -6px;"></div>\n			<div class="anchor" style="left: 463.5px; top: -6px;"></div>\n			<div class="anchor" style="left: 933px; top: -6px;"></div>\n			<div class="anchor" style="left: 933px; top: 325.5px;"></div>\n			<div class="anchor" style="left: 933px; top: 657px;"></div>\n			<div class="anchor" style="left: 463.5px; top: 657px;"></div>\n			<div class="anchor" style="left: -6px; top: 657px;"></div>\n			<div class="anchor" style="left: -6px; top: 325.5px;"></div>\n			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>\n\n			<div class="upperright">\n				<!-- Layer options -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default bringBack" data-uid="0b81edc5c23558f3"><i class="fa fa-toggle-down"></i></button>\n					<button class="btn btn-xs btn-default bringFront" data-uid="0b81edc5c23558f3"><i class="fa fa-toggle-up"></i></button>\n					<button class="btn btn-xs btn-default remove" data-uid="0b81edc5c23558f3"><i class="fa fa-times"></i></button>\n				</div>\n			</div>\n			\n		<img src="http://certifica.dev/pictures/e8693c420641e98f9177dbac38fea4d3.png">\n	\n		</div>\n	\n		<div class="item resizable draggable" data-uid="72d80368d63f100b" data-type="img" data-width="839" data-height="551" data-firstsize="nope" style="width: 369.605px; height: 239.436px; right: auto; bottom: auto; left: 373.27px; top: 69.4607px;">\n			<div class="anchor" style="left: -6px; top: -6px;"></div>\n			<div class="anchor" style="left: 152px; top: -6px;"></div>\n			<div class="anchor" style="left: 310px; top: -6px;"></div>\n			<div class="anchor" style="left: 310px; top: 97.5px;"></div>\n			<div class="anchor" style="left: 310px; top: 201px;"></div>\n			<div class="anchor" style="left: 152px; top: 201px;"></div>\n			<div class="anchor" style="left: -6px; top: 201px;"></div>\n			<div class="anchor" style="left: -6px; top: 97.5px;"></div>\n			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>\n\n			<div class="upperright">\n				<!-- Layer options -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default bringBack" data-uid="72d80368d63f100b"><i class="fa fa-toggle-down"></i></button>\n					<button class="btn btn-xs btn-default bringFront" data-uid="72d80368d63f100b"><i class="fa fa-toggle-up"></i></button>\n					<button class="btn btn-xs btn-default remove" data-uid="72d80368d63f100b"><i class="fa fa-times"></i></button>\n				</div>\n			</div>\n			\n		<img src="http://certifica.dev/pictures/05fe850c77d93b80063db5475dc242db.png">\n	\n		</div>\n	\n		<div class="item resizable draggable" data-uid="e363933c7d64553b" data-type="text" style="width: 669.317px; right: auto; height: 68.4103px; bottom: auto; left: 224.895px; top: 345.129px;">\n\n			<div class="anchor" style="left: -6px; top: -6px;"></div>\n			<div class="anchor" style="left: 278.5px; top: -6px;"></div>\n			<div class="anchor" style="left: 563px; top: -6px;"></div>\n			<div class="anchor" style="left: 563px; top: 25px;"></div>\n			<div class="anchor" style="left: 563px; top: 56px;"></div>\n			<div class="anchor" style="left: 278.5px; top: 56px;"></div>\n			<div class="anchor" style="left: -6px; top: 56px;"></div>\n			<div class="anchor" style="left: -6px; top: 25px;"></div>\n\n			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>\n\n			<div class="upperright">\n\n				<!-- Negritas, italicas, subrayado -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="e363933c7d64553b" data-bold="false"><i class="fa fa-bold"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="e363933c7d64553b" data-italict="false"><i class="fa fa-italic"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="e363933c7d64553b" data-underline="false"><i class="fa fa-underline"></i></button>\n				</div>\n\n				<!-- Font options -->\n				<div class="btn-group">\n\n					<!-- Font family -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<i class="fa fa-font"></i>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="e363933c7d64553b" data-font="helvetica">Helvetica</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-font="arial">Arial</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-font="Open Sans">Open Sans</a></li>\n						</ul>\n					</div>\n\n					<!-- Font size -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<span data-uid="e363933c7d64553b" class="sizeDisplay">24 px</span>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="8">8 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="10">10 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="12">12 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="14">14 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="16">16 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="18">18 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="20">20 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="22">22 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="24">24 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="26">26 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="28">28 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="30">30 px</a></li>\n							<li><a href="#" data-uid="e363933c7d64553b" data-size="32">32 px</a></li>\n						</ul>\n					</div>\n\n					<!-- Font color -->\n					<button class="btn btn-default btn-xs addtext colorpicker-element" data-uid="e363933c7d64553b" data-color="#646464">\n						<i class="fa fa-square" style="color: #646464;"></i>\n					</button>\n				</div>\n\n				<!-- Align -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="e363933c7d64553b" data-align="left"><i class="fa fa-align-left"></i></button>\n					<button class="btn btn-xs btn-default btn-info" data-uid="e363933c7d64553b" data-align="center"><i class="fa fa-align-center"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="e363933c7d64553b" data-align="right"><i class="fa fa-align-right"></i></button>\n				</div>\n\n				<!-- Layer options -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default bringBack" data-uid="e363933c7d64553b"><i class="fa fa-toggle-down"></i></button>\n					<button class="btn btn-xs btn-default bringFront" data-uid="e363933c7d64553b"><i class="fa fa-toggle-up"></i></button>\n					<button class="btn btn-xs btn-default remove" data-uid="e363933c7d64553b"><i class="fa fa-times"></i></button>\n				</div>\n			</div>\n			\n		<div class="editable" data-uid="e363933c7d64553b" contenteditable="" style="font-size: 33px; text-align: center;">[person.name]</div>\n	\n		</div>\n	\n\n\n						<div id="guide-h" class="guide" style="top: 454px; display: none;"></div>\n						<div id="guide-v" class="guide" style="left: 472.469px; display: none;"></div>\n					\n\n\n						<div id="guide-h" class="guide" style="display: none;"></div>\n						<div id="guide-v" class="guide" style="display: none;"></div>\n					\n		<div class="item resizable draggable" data-uid="9eaa64206382ef62" data-type="text" style="width: 647.994px; right: auto; height: 62.5128px; bottom: auto; left: 233.224px; top: 446.473px;">\n\n			<div class="anchor" style="left: -6px; top: -6px;"></div>\n			<div class="anchor" style="left: 269.5px; top: -6px;"></div>\n			<div class="anchor" style="left: 545px; top: -6px;"></div>\n			<div class="anchor" style="left: 545px; top: 22.5px;"></div>\n			<div class="anchor" style="left: 545px; top: 51px;"></div>\n			<div class="anchor" style="left: 269.5px; top: 51px;"></div>\n			<div class="anchor" style="left: -6px; top: 51px;"></div>\n			<div class="anchor" style="left: -6px; top: 22.5px;"></div>\n\n			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>\n\n			<div class="upperright">\n\n				<!-- Negritas, italicas, subrayado -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="9eaa64206382ef62" data-bold="false"><i class="fa fa-bold"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="9eaa64206382ef62" data-italict="false"><i class="fa fa-italic"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="9eaa64206382ef62" data-underline="false"><i class="fa fa-underline"></i></button>\n				</div>\n\n				<!-- Font options -->\n				<div class="btn-group">\n\n					<!-- Font family -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<i class="fa fa-font"></i>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="9eaa64206382ef62" data-font="helvetica">Helvetica</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-font="arial">Arial</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-font="Open Sans">Open Sans</a></li>\n						</ul>\n					</div>\n\n					<!-- Font size -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<span data-uid="9eaa64206382ef62" class="sizeDisplay">24 px</span>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="8">8 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="10">10 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="12">12 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="14">14 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="16">16 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="18">18 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="20">20 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="22">22 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="24">24 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="26">26 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="28">28 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="30">30 px</a></li>\n							<li><a href="#" data-uid="9eaa64206382ef62" data-size="32">32 px</a></li>\n						</ul>\n					</div>\n\n					<!-- Font color -->\n					<button class="btn btn-default btn-xs addtext colorpicker-element" data-uid="9eaa64206382ef62" data-color="#646464">\n						<i class="fa fa-square" style="color: #646464;"></i>\n					</button>\n				</div>\n\n				<!-- Align -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="9eaa64206382ef62" data-align="left"><i class="fa fa-align-left"></i></button>\n					<button class="btn btn-xs btn-default btn-info" data-uid="9eaa64206382ef62" data-align="center"><i class="fa fa-align-center"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="9eaa64206382ef62" data-align="right"><i class="fa fa-align-right"></i></button>\n				</div>\n\n				<!-- Layer options -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default bringBack" data-uid="9eaa64206382ef62"><i class="fa fa-toggle-down"></i></button>\n					<button class="btn btn-xs btn-default bringFront" data-uid="9eaa64206382ef62"><i class="fa fa-toggle-up"></i></button>\n					<button class="btn btn-xs btn-default remove" data-uid="9eaa64206382ef62"><i class="fa fa-times"></i></button>\n				</div>\n			</div>\n			\n		<div class="editable" data-uid="9eaa64206382ef62" contenteditable="" style="text-align: center; font-size: 33px;">[person.anexo1]</div>\n	\n		</div>\n	\n		<div class="item resizable draggable" data-uid="2235e78a12387c9b" data-type="text" style="width: 430.021px; right: auto; height: 61.3333px; bottom: auto; left: 100.249px; top: 534.824px;">\n\n			<div class="anchor" style="left: -6px; top: -6px;"></div>\n			<div class="anchor" style="left: 177.5px; top: -6px;"></div>\n			<div class="anchor" style="left: 361px; top: -6px;"></div>\n			<div class="anchor" style="left: 361px; top: 22px;"></div>\n			<div class="anchor" style="left: 361px; top: 50px;"></div>\n			<div class="anchor" style="left: 177.5px; top: 50px;"></div>\n			<div class="anchor" style="left: -6px; top: 50px;"></div>\n			<div class="anchor" style="left: -6px; top: 22px;"></div>\n\n			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>\n\n			<div class="upperright">\n\n				<!-- Negritas, italicas, subrayado -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="2235e78a12387c9b" data-bold="false"><i class="fa fa-bold"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="2235e78a12387c9b" data-italict="false"><i class="fa fa-italic"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="2235e78a12387c9b" data-underline="false"><i class="fa fa-underline"></i></button>\n				</div>\n\n				<!-- Font options -->\n				<div class="btn-group">\n\n					<!-- Font family -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<i class="fa fa-font"></i>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="2235e78a12387c9b" data-font="helvetica">Helvetica</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-font="arial">Arial</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-font="Open Sans">Open Sans</a></li>\n						</ul>\n					</div>\n\n					<!-- Font size -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<span data-uid="2235e78a12387c9b" class="sizeDisplay">24 px</span>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="8">8 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="10">10 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="12">12 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="14">14 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="16">16 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="18">18 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="20">20 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="22">22 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="24">24 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="26">26 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="28">28 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="30">30 px</a></li>\n							<li><a href="#" data-uid="2235e78a12387c9b" data-size="32">32 px</a></li>\n						</ul>\n					</div>\n\n					<!-- Font color -->\n					<button class="btn btn-default btn-xs addtext colorpicker-element" data-uid="2235e78a12387c9b" data-color="#646464">\n						<i class="fa fa-square" style="color: #646464;"></i>\n					</button>\n				</div>\n\n				<!-- Align -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="2235e78a12387c9b" data-align="left"><i class="fa fa-align-left"></i></button>\n					<button class="btn btn-xs btn-default btn-info" data-uid="2235e78a12387c9b" data-align="center"><i class="fa fa-align-center"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="2235e78a12387c9b" data-align="right"><i class="fa fa-align-right"></i></button>\n				</div>\n\n				<!-- Layer options -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default bringBack" data-uid="2235e78a12387c9b"><i class="fa fa-toggle-down"></i></button>\n					<button class="btn btn-xs btn-default bringFront" data-uid="2235e78a12387c9b"><i class="fa fa-toggle-up"></i></button>\n					<button class="btn btn-xs btn-default remove" data-uid="2235e78a12387c9b"><i class="fa fa-times"></i></button>\n				</div>\n			</div>\n			\n		<div class="editable" data-uid="2235e78a12387c9b" contenteditable="" style="text-align: center; font-size: 33px;">[person.anexo2]</div>\n	\n		</div>\n	\n		<div class="item resizable draggable" data-uid="7c1a0136685efb4c" data-type="text" style="width: 433.575px; right: auto; height: 63.6923px; bottom: auto; left: 586.948px; top: 533.644px;">\n\n			<div class="anchor" style="left: -6px; top: -6px;"></div>\n			<div class="anchor" style="left: 179px; top: -6px;"></div>\n			<div class="anchor" style="left: 364px; top: -6px;"></div>\n			<div class="anchor" style="left: 364px; top: 23px;"></div>\n			<div class="anchor" style="left: 364px; top: 52px;"></div>\n			<div class="anchor" style="left: 179px; top: 52px;"></div>\n			<div class="anchor" style="left: -6px; top: 52px;"></div>\n			<div class="anchor" style="left: -6px; top: 23px;"></div>\n\n			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>\n\n			<div class="upperright">\n\n				<!-- Negritas, italicas, subrayado -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="7c1a0136685efb4c" data-bold="false"><i class="fa fa-bold"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="7c1a0136685efb4c" data-italict="false"><i class="fa fa-italic"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="7c1a0136685efb4c" data-underline="false"><i class="fa fa-underline"></i></button>\n				</div>\n\n				<!-- Font options -->\n				<div class="btn-group">\n\n					<!-- Font family -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<i class="fa fa-font"></i>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-font="helvetica">Helvetica</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-font="arial">Arial</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-font="Open Sans">Open Sans</a></li>\n						</ul>\n					</div>\n\n					<!-- Font size -->\n					<div class="btn-group">\n						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\n							<span data-uid="7c1a0136685efb4c" class="sizeDisplay">24 px</span>\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="8">8 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="10">10 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="12">12 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="14">14 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="16">16 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="18">18 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="20">20 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="22">22 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="24">24 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="26">26 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="28">28 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="30">30 px</a></li>\n							<li><a href="#" data-uid="7c1a0136685efb4c" data-size="32">32 px</a></li>\n						</ul>\n					</div>\n\n					<!-- Font color -->\n					<button class="btn btn-default btn-xs addtext colorpicker-element" data-uid="7c1a0136685efb4c" data-color="#646464">\n						<i class="fa fa-square" style="color: #646464;"></i>\n					</button>\n				</div>\n\n				<!-- Align -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default" data-uid="7c1a0136685efb4c" data-align="left"><i class="fa fa-align-left"></i></button>\n					<button class="btn btn-xs btn-default btn-info" data-uid="7c1a0136685efb4c" data-align="center"><i class="fa fa-align-center"></i></button>\n					<button class="btn btn-xs btn-default" data-uid="7c1a0136685efb4c" data-align="right"><i class="fa fa-align-right"></i></button>\n				</div>\n\n				<!-- Layer options -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default bringBack" data-uid="7c1a0136685efb4c"><i class="fa fa-toggle-down"></i></button>\n					<button class="btn btn-xs btn-default bringFront" data-uid="7c1a0136685efb4c"><i class="fa fa-toggle-up"></i></button>\n					<button class="btn btn-xs btn-default remove" data-uid="7c1a0136685efb4c"><i class="fa fa-times"></i></button>\n				</div>\n			</div>\n			\n		<div class="editable" data-uid="7c1a0136685efb4c" contenteditable="" style="text-align: center; font-size: 33px;">[person.anexo3]</div>\n	\n		</div>\n	', 0, '2016-07-12 06:30:16', '2016-07-12 11:30:16'),
(6, '577822fab897e', '', '\n						<div class="nocanvas"></div>\n						\n						\n						\n					\n					\n		\n	\n\n\n						<div id="guide-h" class="guide"></div>\n						<div id="guide-v" class="guide"></div>\n					\n		<div class="item resizable draggable" data-uid="91a1963a3adae8e2" data-type="img" data-width="839" data-height="551" data-firstsize="nope" style="width: 369.605px; height: 241.795px; left: 0px; top: 0px;">\n			<div class="anchor" style="left: -6px; top: -6px;"></div>\n			<div class="anchor" style="left: 152px; top: -6px;"></div>\n			<div class="anchor" style="left: 310px; top: -6px;"></div>\n			<div class="anchor" style="left: 310px; top: 98.5px;"></div>\n			<div class="anchor" style="left: 310px; top: 203px;"></div>\n			<div class="anchor" style="left: 152px; top: 203px;"></div>\n			<div class="anchor" style="left: -6px; top: 203px;"></div>\n			<div class="anchor" style="left: -6px; top: 98.5px;"></div>\n			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>\n\n			<div class="upperright">\n				<!-- Layer options -->\n				<div class="btn-group">\n					<button class="btn btn-xs btn-default bringBack" data-uid="91a1963a3adae8e2"><i class="fa fa-toggle-down"></i></button>\n					<button class="btn btn-xs btn-default bringFront" data-uid="91a1963a3adae8e2"><i class="fa fa-toggle-up"></i></button>\n					<button class="btn btn-xs btn-default remove" data-uid="91a1963a3adae8e2"><i class="fa fa-times"></i></button>\n				</div>\n			</div>\n			\n		<img src="http://certifica.dev/pictures/05fe850c77d93b80063db5475dc242db.png">\n	\n		</div>\n	', 0, '2016-07-10 21:09:51', '2016-07-11 02:09:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_users`
--

CREATE TABLE `im_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `uid` char(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` char(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `im_users`
--

INSERT INTO `im_users` (`id`, `uid`, `name`, `email`, `password`, `avatar`, `created_at`, `updated_at`, `remember_token`, `permission`) VALUES
(1, '56303784af2c2', 'Daniel García', 'admin@mail.com', '$2y$10$.QBZxgbBoA1sYfnmgq31l.F//.c.qqJHau6ZbtN.UqwXOmPx96Hdm', '563037535d936', '2016-07-12 07:12:31', '2016-07-12 12:12:31', 'nwooSin0QzQXawyi2i6pcsqeFIKhWOxushwGQdE6jpAzUXvatcHTdYcvulvk', ''),
(2, '5639b6540ac15', 'Usuario Demo', 'demo@mail.com', '$2y$10$qmQGfuQCfTXKlom4DsGBF.nJ9080SMLSH5S7f/OMqR7oEoJKLmTDC', 'avatar', '2016-06-20 17:46:50', '2016-06-15 22:23:41', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `im_certificates`
--
ALTER TABLE `im_certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `im_documents`
--
ALTER TABLE `im_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `im_password_resets`
--
ALTER TABLE `im_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `im_pictures`
--
ALTER TABLE `im_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `im_settings`
--
ALTER TABLE `im_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `im_signs`
--
ALTER TABLE `im_signs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `im_templates`
--
ALTER TABLE `im_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `im_users`
--
ALTER TABLE `im_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `im_certificates`
--
ALTER TABLE `im_certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `im_documents`
--
ALTER TABLE `im_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `im_pictures`
--
ALTER TABLE `im_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `im_settings`
--
ALTER TABLE `im_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `im_signs`
--
ALTER TABLE `im_signs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `im_templates`
--
ALTER TABLE `im_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `im_users`
--
ALTER TABLE `im_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
