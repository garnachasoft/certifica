@extends('template')

@section('breadcrumb')
	<li>Inicio</li>
	<li><a href="{{route('documents.index')}}">Documentos</a></li>
	<li class="active">Editar</li>
	<li>{{$certificate[0]->nombre}}</li>
@stop

@section('styles-oneui')
    <link rel="stylesheet" href="{{URL::asset('/oneui/js/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('/oneui/js/plugins/select2/select2-bootstrap.min.css')}}">
@stop

@section('scripts')
    <script src="{{URL::asset('/oneui/js/plugins/select2/select2.full.min.js')}}"></script>
    <script>
        jQuery(function () {
            App.initHelpers(['select2']);

            $('#_signs').change(function () {
            	if ( $(this).val() != null ) {
            		$('#signs').val( $(this).val().join('|') );
            	} else {
            		$('#signs').val('');
            	}
            });
        });
    </script>
@stop

@section('content')
	<div class="content">
		<!-- Formulario -->
		
		<form action="{{route('certificates.update', ['uid' => $certificate[0]->uid])}}" method="POST">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-lg-6 col-sm-8 col-xs-12 col-lg-offset-3 col-sm-offset-2">
				<div class="block">
					<div class="block-header bg-gray-lighter">
						<h3 class="block-title">Datos del Certificado</h3>
					</div>
					<div class="block-content">
						<div class="form-group">
							<div class="form-material">
								<label>Nombre</label>
								<input class="form-control input-lg" type="text" name="nombre" placeholder="Nombre" value="{{$certificate[0]->nombre}}">
							</div>
						</div>

						<div class="form-group">
							<label>Email</label>
							<input class="form-control" type="email" name="email" placeholder="Email" value="{{$certificate[0]->email}}">

						</div>
						<div class="form-group">
							<label>Rut</label>
							<input type="text" class="form-control" name="rut" placeholder="Rut" value="{{$certificate[0]->rut}}">
						</div>

						<div class="form-group">
							<label>Teléfono</label>
							<input type="text" class="form-control" name="telefono" placeholder="Rut" value="{{$certificate[0]->telefono}}">
						</div>

						<div class="form-group" style="border-top: 1px solid #eee; margin-top: 20px; padding-top: 20px;">
							<button class="btn btn-primary" type="submit">Guardar</button>
							<a href="{{ route('documents.view', [$document] )}}" class="btn btn-default" role="button">Cancelar</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		</form>

	</div>
@stop

@section('modals')

	@if($errors->has())
		<?php $dis = '' ?>
		@foreach ($errors->all() as $error)
			<?php $dis .= "<li>{$error}</li>" ?>
		@endforeach

		<div class="modal" id="modal-errors" tabindex="-1" role="dialog" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="block block-themed remove-margin-b">
		                <div class="block-header bg-danger">
		                    <ul class="block-options">
		                        <li>
		                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
		                        </li>
		                    </ul>
		                    <h3 class="block-title">Verfica lo siguiente</h3>
		                </div>
		                <div class="block-content">
		                	<ul>
								{{$dis}}
							</ul>
						</div>
		            </div>
		            <div class="modal-footer">
		                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal">Aceptar</button>
		            </div>
		        </div>
		    </div>
		</div>
	@endif
@stop