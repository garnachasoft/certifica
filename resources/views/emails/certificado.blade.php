<html>
<body>
<p>Hola {{$person->nombre}}!, para poder descargar tu certificado sólo has click en el enlace de abajo.
<br /><br />
<a href="{{route('certificates.download', ['uid' => $person->uid])}}"">Click aquí para descargar</a>
</p>
</body>
</html>