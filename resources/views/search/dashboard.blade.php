@extends('template')

@section('breadcrumb')
        <li>Inicio</li>
        <li class="active">Search</li>
@stop

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.1.3/mustache.min.js"></script>
	<script>
		var template = $('#template').html();
		Mustache.parse(template);

		$(function () {
			$('form').submit(function (e) {
				e.preventDefault();
			});
			$('#rut').on('keypress', function (e) {
				$.get('/search/by/rut', { rut: $(this).val() }, function (res) {
					$('#results').empty();
					if (res.data.length > 0) {
						$('#empty').hide();

						$.each(res.data, function(i, item) {
		                	var render = Mustache.render(template, item);
		                	$('#results').append(render);
						});
					} else {
						$('#empty').show();
					}
				});
			});

		});
	</script>
@stop

@section('content')

	<script id="template" type="text/html">
        <a class="block block-link-hover3" href="/certificates/@{{uid}}/download">
            <div class="block-content block-content-full clearfix">
                <h1 class="h4 text-info">@{{nombre}}</h1>
                <h2 class="h5 text-success">@{{rut}}</h2>
            </div>
        </a>
	</script>

	<div class="content">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
                <form>
                    <div class="input-group input-group-lg">
                        <div class="input-group-addon">Buscar</div>
                        <input id="rut" class="js-icon-search form-control" type="text" placeholder="ej: XX.XXX.XXX-Y">
                    </div>
                </form>
			</div>
		</div>	
	</div>	
	<div class="content">	
		<div id="empty" class="row">
			<div class="col-sm-8 col-sm-offset-2">
                <a  class="block block-link-hover3" href="{{route('documents.new')}}">
                    <div class="block-content block-content-full clearfix text-center">
                        <h1 class="h4">No hay ningún resultado</h1>
                    </div>
                </a>
			</div>
		</div>	
		<div class="row">
			<div id="results" class="col-sm-8 col-sm-offset-2">
			</div>
		</div>
	</div>
@endsection


