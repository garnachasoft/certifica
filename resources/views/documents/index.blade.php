@extends('template')

@section('breadcrumb')
        <li>Inicio</li>
        <li class="active">Documentos</li>
@stop
@section('content')
	<div class="content">
		<div class="btn-group">
			<a href="{{route('documents.new')}}" class="btn btn-default" role="button">Nuevo documento</a>
		</div>
	</div>

	<div class="content">
		<div class="row">
		@forelse($documents as $d)

			<div class="col-sm-6 col-md-4 col-lg-3">
				<a class="block block-link-hover2" href="{{route('documents.view', ['uid' => $d->uid])}}">
					<div class="block-content block-content-full text-center bg-image">

						<h1 class="h3">{{$d->name}}</h1>

					</div>
				</a>
			</div>
			
		@empty

            <div class="col-lg-12">
                <a class="block block-link-hover3" href="{{route('documents.new')}}">
                    <div class="block-content block-content-full clearfix text-center">
                        <h1 class="h4 push-20-t">Aún no hay ningún documento</h1>
                        <p>Crea tu primer documento picando aquí</p>
                    </div>
                </a>
            </div>

		@endforelse
		</div>
	</div>
@endsection


