@extends('template')

@section('breadcrumb')
	<li>Inicio</li>
	<li><a href="{{route('documents.index')}}">Documentos</a></li>
	<li class="active">Ver</li>
	<li>{{$document->name}}</li>
@stop


@section('scripts')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/4.0.6/sweetalert2.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/4.0.6/sweetalert2.js"></script>
    <script src="{{URL::asset('/oneui/js/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
	<script>

		var notifyOptions = {
			element: 'body',
			type: "info",
			allow_dismiss: true,
			placement: {
				from: "bottom",
				align: "left"
			},
			offset: 20,
			spacing: 10,
			z_index: 1031,
			timer: 1000,
			animate: {
				enter: 'animated fadeInUp',
				exit: 'animated fadeOutDown'
			},
			icon_type: 'class'
		};
		$(function () {

			$('.certificates-import').on('click', function (e) {
				e.preventDefault();
				$('#excel_import').trigger('click');
			});


			$('#excel_import').change(function(event) {
				$('#upload_excel').submit();
			});

			// Notificar a todos
			$('[data-notifyAll]').on('click', function () {
				$.post('/document/{{$document->uid}}/notify/all', {}, function () {
					var data = {}; data.message = '<i class="fa fa-send"></i> Se han enviado todas las notificaciones por correo';
					notifyOptions.type = 'success';
					$.notify(data, notifyOptions);
				}).fail(function () {
					var data = {}; data.message = '<i class="fa fa-send"></i> No se pudieron enviar las notificaciones';
					notifyOptions.type = 'danger';
					$.notify(data, notifyOptions);
				});
			});
			
			// Notificar a uno
			$('[data-notify]').on('click', function () {
				var uid = $(this).data('uid');
				var name = $(this).data('name');

				$.post('/document/{{$document->uid}}/notify/one/' + uid, {}, function () {
					var data = {}; data.message = '<i class="fa fa-send"></i> Se ha enviado una notificación por correo a ' + name;
					notifyOptions.type = 'success';
					$.notify(data, notifyOptions);
				}).fail(function () {
					var data = {}; data.message = '<i class="fa fa-send"></i> No se ha podido notificar a ' + name;
					notifyOptions.type = 'danger';
					$.notify(data, notifyOptions);
				});
			});

			$('.confirm').click(function(e){
				e.preventDefault();
				var url = $(this).attr('href');
				swal({
					title: '¿Estás seguro?',
					text: 'No podrás revertir esta acción',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					cancelButtonText: 'No, cancelar',
					confirmButtonText: 'Si, borrar'
				}).then(function() {
					window.location.href = url;
				});
			});

			$('#all').change(function(){
				if( $(this).is(':checked') ){
					$('[name="certificates[]"]').prop('checked', true);
				}else{
					$('[name="certificates[]"]').prop('checked', false);
				}
			});

			$('.massive').click(function(e){
				e.preventDefault();
				var url = $(this).attr('href');
				swal({
					title: '¿Estás seguro?',
					text: 'No podrás revertir esta acción',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					cancelButtonText: 'No, cancelar',
					confirmButtonText: 'Si, borrar'
				}).then(function() {
					$('form').submit();
				});
			});

		});
	</script>
@stop

@section('content')

	<div style="display: none;">
		<form id="upload_excel" action="{{route('certificates.import')}}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" name="document" value="{{$document->uid}}">
			<input id="excel_import" type="file" name="excel_import" />
		</form>
	</div>

	<div class="content">
		<a href="#" class="btn btn-success" role="button" data-notifyAll><i class="fa fa-send"></i> Notificar a todos ({{$certificates->count()}})</a>
		<a href="#" class="btn btn-default certificates-import" role="button"><i class="fa fa-folder-open"></i> Importar registros</a>


		<div class="btn-group pull-right push-10-l">
			<a href="{{route('documents.edit', ['uid' => $document->uid])}}" class="btn btn-default" role="button"><i class="fa fa-edit"></i> Configurar documento</a>
			<a href="{{route('documents.delete', ['uid' => $document->uid])}}" class="btn btn-danger" role="button"><i class="fa fa-trash"></i> Eliminar documento</a>
		</div>
		<a class="btn btn-default pull-right" href="{{route('documents.template.editor', ['uid' => $document->uid])}}" role="button"><i class="fa fa-magic"></i> Editar plantilla</a>
	</div>

	<form method="post" action="{{ route('certificates.massive.delete') }}">
		<div class="content">
			{{ csrf_field() }}
			<button class="btn btn-default massive" role="button">
				<i class="fa fa-folder-open"></i>
				Eliminar seleccionados
			</button>
		</div>

		<div class="content">

			<div class="row">

				@if($certificates->total() != 0)
					<div class="col-lg-12">
						<div class="block">
							<div class="block-content block-content-full clearfix remove-padding">
								<div class="table-responsive">
									<table class="table table-striped table-borderless table-header-bg">
										<thead>
											<tr>
												<th>
													<input id="all" type="checkbox" name="certificates">
												</th>
												<th>RUT</th>
												<th>Nombre</th>
												<th>Email</th>
												<th>Anexo 1</th>
												<th>Anexo 2</th>
												<th>Anexo 3</th>
												<th class="text-center">Options</th>
											</tr>
										</thead>
										<tbody>

											@foreach($certificates as $c)
												<tr>
													<td>
														<input type="checkbox" name="certificates[]" value="{{ $c->id }}">
													</td>
													<td>{{$c->rut}}</td>
													<td>{{$c->nombre}}</td>
													<td>{{$c->email}}</td>
													<td>{{$c->anexo1}}</td>
													<td>{{$c->anexo2}}</td>
													<td>{{$c->anexo3}}</td>
													<td class="text-center">
														<button class="btn btn-xs btn-success" type="button" data-toggle="tooltip" title="Notificar por email" data-notify data-name="{{$c->nombre}}" data-uid="{{$c->uid}}"><i class="fa fa-send"></i></button>
														<div class="btn-group push-5-l">
															<a href="{{ route('certificates.download', [ $c->uid ]) }}" class="btn btn-xs btn-default" data-toggle="tooltip" title="Descargar Certificado"><i class="fa fa-paperclip"></i></a>
															<a href="{{ route('certificates.edit', [ $c->uid ]) }}" class="btn btn-xs btn-default" data-toggle="tooltip" title="Editar registro"><i class="fa fa-pencil"></i></a>
															<a href="{{ route('certificates.delete', [ $c->uid ]) }}" class="confirm btn btn-xs btn-default" data-toggle="tooltip" title="Eliminar registro"><i class="fa fa-times"></i></a>
														</div>
													</td>
												</tr>
											@endforeach

											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				@else

		            <div class="col-lg-12">
		                <a class="block block-link-hover3 certificates-import" href="#">
		                    <div class="block-content block-content-full clearfix text-center">
		                        <h1 class="h4 push-20-t">Aún no hay ningún registro</h1>
		                        <p>Click aquí para importar nuevos registros</p>
		                    </div>
		                </a>
		            </div>

				@endif

		        <div class="col-xs-12">
		            <div class="pagination">{{$certificates->links()}}</div>
		        </div>
			</div>

		</div>
	</form>
@endsection


