@extends('template')

@section('breadcrumb')
	<li>Inicio</li>
	<li><a href="{{route('documents.index')}}">Documentos</a></li>
	<li class="active">Nuevo</li>
@stop

@section('styles-oneui')
    <link rel="stylesheet" href="{{URL::asset('/oneui/js/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('/oneui/js/plugins/select2/select2-bootstrap.min.css')}}">
@stop

@section('scripts')
    <script src="{{URL::asset('/oneui/js/plugins/select2/select2.full.min.js')}}"></script>
    <script>
        jQuery(function () {
            App.initHelpers(['select2']);

            $('#_signs').change(function () {
            	if ( $(this).val() != null ) {
            		$('#signs').val( $(this).val().join('|') );
            	} else {
            		$('#signs').val('');
            	}
            });
        });
    </script>
@stop

@section('content')
	<div class="content">
		<!-- Formulario -->
		
		<form action="{{route('documents.create')}}" method="POST">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-lg-6 col-sm-8 col-xs-12 col-lg-offset-3 col-sm-offset-2">
				<div class="block">
					<div class="block-header bg-gray-lighter">
						<h3 class="block-title">Datos del documento</h3>
					</div>
					<div class="block-content">
						<div class="form-group">
							<div class="form-material">
								<label>Nombre</label>
								<input class="form-control input-lg" type="text" name="name" placeholder="Nombre" value="{{old('name')}}">
							</div>
						</div>

						<div class="form-group">
							<label>Descripción</label>
							<input class="form-control" type="text" name="description" placeholder="Descripción" value="{{old('description')}}">

						</div>
						<div class="form-group">
							<label>Notas</label>
							<textarea class="form-control" name="notes" placeholder="Notas...">{{old('notes')}}</textarea>
						</div>

						<div class="form-group">
                            <label>Firmas</label>

                            <select class="js-select2 form-control" id="_signs" name="_signs" style="width: 100%;" data-placeholder="Selecciona firmas.." multiple>
                                <option></option>
								@forelse($signs as $s)
									<option value="{{$s->uid}}">{{$s->name}}</option>
								@empty
						        	<option>Sin firmas...</option>
								@endforelse
                            </select>
                            <input type="hidden" id="signs" name="signs" value="{{old('signs')}}"/>

						</div>


						<div class="form-group" style="border-top: 1px solid #eee; margin-top: 20px; padding-top: 20px;">
							<button class="btn btn-primary" type="submit">Añadir</button>
							<a href="{{route('documents.index')}}" class="btn btn-default" role="button">Cancelar</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		</form>

	</div>
@stop

@section('modals')

	@if($errors->has())
		<?php $dis = '' ?>
		@foreach ($errors->all() as $error)
			<?php $dis .= "<li>{$error}</li>" ?>
		@endforeach

		<div class="modal" id="modal-errors" tabindex="-1" role="dialog" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="block block-themed remove-margin-b">
		                <div class="block-header bg-danger">
		                    <ul class="block-options">
		                        <li>
		                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
		                        </li>
		                    </ul>
		                    <h3 class="block-title">Verfica lo siguiente</h3>
		                </div>
		                <div class="block-content">
		                	<ul>
								{{$dis}}
							</ul>
						</div>
		            </div>
		            <div class="modal-footer">
		                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal">Aceptar</button>
		            </div>
		        </div>
		    </div>
		</div>
	@endif
@stop