<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Login | Dashboard</title>

        <meta name="author" content="@dannegm">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
        
        <link rel="shortcut icon" href="{{URL::asset('/oneui/img/favicons/favicon.png')}}">

        <link rel="icon" type="image/png" href="{{URL::asset('/oneui/img/favicons/favicon-16x16.png')}}" sizes="16x16'">
        <link rel="icon" type="image/png" href="{{URL::asset('/oneui/img/favicons/favicon-32x32.png')}}" sizes="32x32'">
        <link rel="icon" type="image/png" href="{{URL::asset('/oneui/img/favicons/favicon-96x96.png')}}" sizes="96x96'">
        <link rel="icon" type="image/png" href="{{URL::asset('/oneui/img/favicons/favicon-160x160.png')}}" sizes="160x160">
        <link rel="icon" type="image/png" href="{{URL::asset('/oneui/img/favicons/favicon-192x192.png')}}" sizes="192x192">

        <link rel="apple-touch-icon" sizes="57x57" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{URL::asset('/oneui/img/favicons/apple-touch-icon-180x180.png')}}">

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <link rel="stylesheet" href="{{URL::asset('/oneui/css/bootstrap.min.css')}}">
        <link rel="stylesheet" id="css-main" href="{{URL::asset('/oneui/css/oneui.min.css')}}">

        <style>
            #title {
                font-size: 5em;
                letter-spacing: .2em;

            }
        </style>

    </head>
    <body class="bg-themed bg-primary-dark">

        <script id="template" type="text/html">
            <div class="col-sm-3">
                <a class="block block-link-hover3" href="/certificates/@{{uid}}/download">
                    <div class="block-content block-content-full clearfix">
                        <h1 class="h4 text-info">@{{nombre}}</h1>
                        <h2 class="h5 text-success">@{{rut}}</h2>
                    </div>
                </a>
            </div>
        </script>


        <div id="main" class="content content-full overflow-hidden bg-image remove-padding" style="background-image: url('https://images.unsplash.com/photo-1464655646192-3cb2ace7a67e?dpr=1&auto=format&crop=entropy&fit=crop&w=1440&h=960&q=80');">
            <div class="row remove-padding remove-padding">
                <div class="col-sm-9">
                    <div class="content push-150-t">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <h1 id="title" class="text-white-op">
                                    <img style="max-width: 100%; padding: 20px;" src="https://www.mecertifico.cl/wp-content/uploads/2016/06/logo-transparente.png">
                                </h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <form id="search">
                                    <div class="input-group input-group-lg">
                                        <div class="input-group-addon">Buscar</div>
                                        <input id="rut" class="js-icon-search form-control" type="text" placeholder="ej: XX.XXX.XXX-Y">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row push-20-t">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="row" id="results">
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3 remove-padding remove-padding">
                    <div class="remove-margin block block-themed full-height">
                        <div class="block-content block-content-full block-content-narrow">
                            <h1 class="h2 font-w600 push-150-t push-5">Dashboard</h1>
                            <p>Bienvenido, por favor inicia sesión.</p>

                            <form method="POST" action="{{ url('/login') }}" id="login" role="form">
                                {{ csrf_field() }}
                                <div class="js-validation-login form-horizontal push-30-t push-50" action="index.html" method="post">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="form-material form-material-primary">
                                                <input class="form-control" type="text" name="email" value="{{ old('email') }}">
                                                <label for="login-email">E-Mail</label>
                                            </div>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="form-material form-material-primary">
                                                <input class="form-control" type="password" name="password">
                                                <label for="login-password">Contraseña</label>
                                            </div>
                                                        
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-6">
                                            <label class="css-input css-checkbox css-checkbox-primary">
                                                <input type="checkbox" name="remember" checked><span></span> Recordar
                                            </label>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-block btn-primary" type="submit"> Entrar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Scripts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/mustache.js/2.1.3/mustache.min.js"></script>
        <script src="{{URL::asset('/oneui/js/oneui.min.js')}}"></script>
        <script>
            var page = {
                resize: function () {
                    var height = $(window).height();
                    $('#main, .full-height').height(height);
                },
                events: function () {
                    $(window).resize(page.resize);
                },
                init: function () {
                    page.resize();
                }
            };
            $(page.init);
        </script>
        <script>
            var template = $('#template').html();
            Mustache.parse(template);

            $(function () {
                $('#search').submit(function (e) {
                    e.preventDefault();
                });
                $('#rut').on('keyup', function (e) {
                    var val = $(this).val();
                    if( val.length > 3 ){
                        $.get('/search/by/rut', { rut: $(this).val() }, function (res) {
                            $('#results').empty();
                            if (res.data.length > 0) {
                                $.each(res.data, function(i, item) {
                                    var render = Mustache.render(template, item);
                                    $('#results').append(render);
                                });
                            }
                        });
                    }else{
                        $('#results').empty();
                        $('#results').html('<h2>No hay resultados</h2>');
                    }
                });

            });
        </script>
        <style>
            .content p, .content .push, .content .block, .content .items-push>div{
                min-height: 120px;
            }
        </style>
    </body>
</html>