<!DOCTYPE html>
<html class="no-focus">
<head>
	<meta charset="utf-8">
	<title>Preview PDF</title>

	<meta name="author" content="@dannegm">
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
	<link rel="stylesheet" href="{{URL::asset('/oneui/css/bootstrap.min.css')}}">
	<link rel="stylesheet" id="css-main" href="{{URL::asset('/oneui/css/oneui.min.css')}}">

	<style>
		body {
			background: #fff;
		}
		#page-viewer {
			background: #fff;
			width: 1110px;
			height: 782px;
			overflow: hidden;
		}
		
		#page-viewer2 {
			background: #fff;
			width: 1110px;
			height: 750px;
			overflow: hidden;
		}

		#page-viewer .item {
			display: block;
			padding: 10px;
			text-align: left;
			position: absolute;
			width: 50%;
			height: 20%;
		}

		#page-viewer .item .upperright {
			display: none;
		}
		#page-viewer .item .movee {
			display: none;
		}

		#page-viewer .item .btn {
			display: none;
		}

		#page-viewer .item .editable {
			font-weight: normal;
			font-size: 40px;
			line-height: 1.2;
		}


		#page-viewer .item img {
			width: 100%;
			height: 100%;
			display: block;
		}
		@page {
			size: A4 landscape;
			padding: 0;
			margin: 0;
			page-break-inside: avoid;
		}
	</style>

</head>
<body>
	<div id="page-viewer">

		<!-- Page Content -->

		{!! $template->layout !!}

	</div>
	<div id="page-viewer2">
		
		<div class="text-center">
			<br /><br /><br /><br /><br /><br />
			<img src="http://www.mecertifico.cl/wp-content/uploads/2016/06/logo-transparente.png" />
			<p>Este certificado fue expedido a traves de la plataforma me certifico.cl</p>
			<p>Para comprobar la validez de este certificado por favor visite el siguiente enlace <a href="http://plataforma.mecertifico.cl/document/[uid]/template/preview">http://plataforma.mecertifico.cl/document/[uid]/template/preview</a></p>
			<p>O en su defecto, coloque este código en el buscador de la plataforma <b>[person.rut]</b></p>
		</div>

	</div>
</body>
</html>