@extends('template')

@section('breadcrumb')
	<li>Inicio</li>
	<li><a href="{{route('documents.index')}}">Documentos</a></li>
	<li>Plantilla</li>
	<li class="active">Editar</li>
	<li>{{$document->name}}</li>
@stop


@section('styles-oneui')
	<link rel="stylesheet" href="{{URL::asset('/oneui/js/plugins/magnific-popup/magnific-popup.min.css')}}">
	<link rel="stylesheet" href="{{URL::asset('/oneui/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
@stop

@section('styles')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

	<style>
		#editor-container {
			background: #444;
			widows: auto;
			min-height: 800px;
			box-shadow: inset 1px 1px 1px 1px rgba(0,0,0,.5), inset -1px -1px 1px 1px rgba(0,0,0,.5);
			overflow: hidden;
			overflow: hidden;
		}
		#page-viewer, #waiting {
			background: #fff;
			border: 1px solid rgba(255,255,255,.5);
			width: 297px;
			height: 210px;
			box-shadow: 1px 1px 10px 2px rgba(0,0,0,.5);
			position: absolute;
			left: 50%;
		}

		#waiting {
			background: #2c343f;
		}

		#page-viewer .item {
			outline: 1px solid rgba(216, 216, 216, 1.00);
			display: inline-block;
			padding: 10px;
			text-align: left;
			position: absolute;
			width: 60%;
			height: 20%;
			background: rgba(255,255,255,.3);
		}
		.item.focus {
			outline: 3px solid rgba(43, 151, 232, 1.00) !important;
		}

		.item .anchor {
			position: absolute;
			display: none;
			width: 8px;
			height: 8px;
			background: rgba(43, 151, 232, 1.00);
			border: 1px solid #fff;
		}
		.item.focus .anchor {
			display: block;
		}

		.item.success {
			outline: 3px solid rgba(70, 195, 123, 1.00) !important;
		}
		.item.success .anchor {
			background: rgba(70, 195, 123, 1.00) !important;
		}

		.item .ui-icon {
			background: none !important;
		}


		.item:hover .upperright{
			opacity: 1 !important;
			z-index: 9999;
		}
		#page-viewer .item .upperright {
			position: absolute;
			margin: 0;
			margin-top: 0;
			right: 0;
			top: -28px;
			line-height: 0;
			opacity: 0;
			-moz-transition: .3s;
			-webkit-transition: .3s;
			transition: .3s;
		}
		#page-viewer .item .movee {
			position: absolute;
			margin: -10px;
		}

		#page-viewer .item .btn {
			opacity: 1;
		}
		#page-viewer .item .btn:hover, #page-viewer .item .btn:active {
			opacity: 1;
		}

		#page-viewer .item .editable {
			line-height: 1.2;
		}

		#page-viewer .item img {
			width: 100%;
			height: 100%;
			display: block;
		}
		#page-viewer .nocanvas {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			display: block;
		}

		.guide {
			display: none; 
			position: absolute; 
			left: 0; 
			top: 0; 
		}

		#guide-h {
			border-top: 1px dotted rgba(219, 27, 84, 1.00); 
			width: 100%; 
		}

		#guide-v {
			border-left: 1px dotted rgba(219, 27, 84, 1.00); 
			height: 100%; 
		}
	</style>
@stop

@section('scripts')
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/mustache.js/2.1.3/mustache.min.js"></script>
	<script src="{{URL::asset('/oneui/js/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
	<script src="{{URL::asset('/oneui/js/plugins/magnific-popup/magnific-popup.min.js')}}"></script>
	<script src="{{URL::asset('/oneui/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{URL::asset('/oneui/js/dnn.upload.js')}}"></script>
	<script src="{{URL::asset('/assets/js/utils.js')}}"></script>
	<script>
		var uid = '{{$document->template}}';

		var pageSize = {
			w: 1110,
			h: 782
		};

		var notifyOptions = {
			element: 'body',
			type: "info",
			allow_dismiss: true,
			placement: {
				from: "bottom",
				align: "left"
			},
			offset: 20,
			spacing: 10,
			z_index: 1031,
			timer: 1000,
			animate: {
				enter: 'animated fadeInUp',
				exit: 'animated fadeOutDown'
			},
			icon_type: 'class'
		};

		var tpl_item = $('#tpl_item').html();
		var tpl_item_text = $('#tpl_item_text').html();
		var tpl_text = $('#tpl_text').html();
		var tpl_src = $('#tpl_src').html();
		var tpl_sign = $('#tpl_sign').html();
		var tpl_picture = $('#tpl_picture').html();

		Mustache.parse(tpl_item);
		Mustache.parse(tpl_item_text);
		Mustache.parse(tpl_text);
		Mustache.parse(tpl_src);
		Mustache.parse(tpl_sign);
		Mustache.parse(tpl_picture);


		var computeGuidesForElement = function (elem, pos, w, h) {
			if (elem != null) {
				var $t = $(elem);
				pos = {
					left: $t.offset().left - $('#page-viewer').offset().left,
					top: $t.offset().top - $('#page-viewer').offset().top
				};
				w = $t.outerWidth() - 1;
				h = $t.outerHeight() - 1; 
			}

			return [
				// Primary
				{ type: "h", left: pos.left, top: pos.top },
				{ type: "v", left: pos.left, top: pos.top },

				// Offset
				{ type: "h", left: pos.left, top: pos.top + h },
				{ type: "v", left: pos.left + w, top: pos.top },

				// you can add _any_ other guides here as well (e.g. a guide 10 pixels to the left of an element)
				// Middles
				{ type: "h", left: pos.left, top: pos.top + h/2 },
				{ type: "v", left: pos.left + w/2, top: pos.top },
			];
		};
		var MIN_DISTANCE = 10;
		var guides = [];
		var innerOffsetX, innerOffsetY;

		var computeAnchrosForElement = function (w, h) {
			var o = 6;
			var i = 2;
			var m = 4;
			return [
				{ type: "nw", left: 0 - o, top: 0 - o },
				{ type: "nm", left: (w/2) - m, top: 0 - o },
				{ type: "ne", left: w - i, top: 0 - o },
				{ type: "em", left: w - i, top: (h/2) - m },
				{ type: "se", left: w - i, top: h - i },
				{ type: "sm", left: (w/2) - m, top: h - i },
				{ type: "sw", left: 0 - o, top: h - i },
				{ type: "wm", left: 0 - o, top: (h/2) - m }
			];
		};
		var buildAnchors = function (elem) {
			$t = $(elem);
			anchors = computeAnchrosForElement( $t.outerWidth(), $t.outerHeight() );

			$.each(anchors, function(i, anchor) {
				var $anchor = $t.children('.anchor').eq(i);
				$anchor.css({
					left: anchor.left,
					top: anchor.top
				});
			});
		}

		$(function () {
			App.initHelpers('magnific-popup');

			// Crear
			var pictureAPI = "{{route('pictures.upload')}}";
			var options_pic = {
				url: pictureAPI,
				filename: 'file',
				group: 'ForTemplate',
				maxSize: 8 * 1024 * 1024,
				maxWidth: 2048,
				start: function () {
					editor.destroy();
				},
				process: function (picture) {
					$('#progress_pic').show();
				},
				error: function (error) {
					$('#error_txt_pic').text(error.message);
					$('#error_pic').fadeIn();
				},
				xhr: function () {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress', function(p) {
						var percentComplete = p.loaded / p.total;
						var percent = parseFloat(Math.round((percentComplete * 100)));
						$('#progressbar_pic').css({
							'width': percent + '%'
						});
					}, false);
					return xhr;
				},
				success: function (response) {
					$('#progress_pic').hide();
					
					var renderImg = Mustache.render(tpl_src, { src: response.pic });
					var renderItem = Mustache.render(tpl_item, { content: renderImg, type: 'img', width: response.width, height: response.height });
					$('#page-viewer')
						.append(renderItem);

					$('#modal-addPicture').modal('hide');

					editor.create();
				}
			};
			$('#picker_pic').on('click', function (e) {
				e.preventDefault();
				$('#file_pic').trigger('click');
			});
			$(document).on('click', '.add-picture', function (e) {
				e.preventDefault();
				$('#modal-addPicture').modal('show');
				$('#error_picture').hide();
			});

			$('#file_pic').on('change', function (e) {
				e.preventDefault();
				options_pic.files = this.files;
				upload( options_pic );
			});
		});

		var editor = {
			create: function () {
				// Layer behaviors
					$('.item').on('click', function () {
						$('.item').removeClass('focus');
						$(this).addClass('focus');
					});

					$('.resizable').resizable({
						handles: 'all',
						create: function () {
							buildAnchors(this);

							$t = $(this);

							if ( $t.data('type') == 'img' && $t.attr('data-firstsize') == 'yep') {
								$t.attr('data-firstsize', 'nope');

								var pageWidth = $('#page-viewer').width();
								var pageHeight = $('#page-viewer').height();

								var nwidth = pageWidth / 3;
								var nheihgt = utils.proporcional($t.data('width'), $t.data('height'), pageWidth / 3);
								
								$t.css({
									width: nwidth,
									height: nheihgt
								});

								buildAnchors(this);
							}
						},
						start: function () {
							buildAnchors(this);
							$t.removeClass('success');
						},
						resize: function (event, ui) {
							buildAnchors(this);
							$t = $(this);
						
							if ( $t.data('type') == 'img' ) {
								var owidth = ui.originalSize.width;
								var oheihgt = ui.originalSize.height;

								var nwidth = ui.size.width;
								var nheihgt = ui.size.height;

								var mustBeHeight = utils.proporcional($t.data('width'), $t.data('height'), nwidth) + 20;
								var mustBeWidth = utils.proporcional($t.data('height'), $t.data('width'), nheihgt) + 20;

								var dist_h = Math.abs(mustBeHeight - nheihgt);
								var dist_w = Math.abs(mustBeWidth - nwidth);

								if ( dist_h <= MIN_DISTANCE && oheihgt != nheihgt && owidth == nwidth ) {
									$t.css({
										height: mustBeHeight
									});
									$t.addClass('success');
								} else if ( dist_w <= MIN_DISTANCE && owidth != nwidth && oheihgt == nheihgt ) {
									$t.css({
										width: mustBeWidth
									});
									$t.addClass('success');
								} else {
									$t.removeClass('success');
								}
							}
						},
						stop: function () {
							buildAnchors(this);
							$t.removeClass('success');
						}
					});

					$('.draggable').draggable({
						cursor: 'move',
						containment: '#editor-container',
						cancel: 'p, .editable',
						start: function (event, ui) {
							guides = $.map( $( ".draggable" ).not( this ), computeGuidesForElement );
							innerOffsetX = event.originalEvent.offsetX;
							innerOffsetY = event.originalEvent.offsetY;
						}, 
						drag: function (event, ui) {
							var guideV,
								guideH,

								distV = MIN_DISTANCE + 1,
								distH = MIN_DISTANCE + 1,

								offsetV,
								offsetH;

							var chosenGuides = { 
								top: { dist: MIN_DISTANCE + 1 },
								left: { dist: MIN_DISTANCE + 1 }
							};

							var $t = $(this); 

							var pos = {
								top: (event.originalEvent.pageY - innerOffsetY) - $('#page-viewer').offset().top,
								left: (event.originalEvent.pageX - innerOffsetX) - $('#page-viewer').offset().left
							};

							var w = $t.outerWidth() - 1; 
							var h = $t.outerHeight() - 1; 
							var elemGuides = computeGuidesForElement (null, pos, w, h); 

							$.each ( guides, function (i, guide) {

								$.each ( elemGuides, function (i, elemGuide) {

									if (guide.type == elemGuide.type) {

										var prop = guide.type == "h"? "top":"left";
										var d = Math.abs (elemGuide[prop] - guide[prop]);

										if (d < chosenGuides[prop].dist) {

											chosenGuides[prop].dist = d; 
											chosenGuides[prop].offset = elemGuide[prop] - pos[prop]; 
											chosenGuides[prop].guide = guide; 

										}

									}
								}); 
							});
							
							if (chosenGuides.top.dist <= MIN_DISTANCE) {
								$( "#guide-h" ).css( "top", chosenGuides.top.guide.top ).show(); 
								ui.position.top = chosenGuides.top.guide.top - chosenGuides.top.offset;
							} else {
								$( "#guide-h" ).hide(); 
								ui.position.top = pos.top; 
							}
							
							if (chosenGuides.left.dist <= MIN_DISTANCE) {
								$( "#guide-v" ).css( "left", chosenGuides.left.guide.left ).show(); 
								ui.position.left = chosenGuides.left.guide.left - chosenGuides.left.offset; 
							} else {
								$( "#guide-v" ).hide(); 
								ui.position.left = pos.left; 
							}
						}, 
						stop: function( event, ui ){
							$( "#guide-v, #guide-h" ).hide(); 
						}
					});

				// Layer toolbar
					$(document).on('click', '.remove', function (e) {
						e.preventDefault();
						var uid = $(this).data('uid');
						$('.item[data-uid="' + uid + '"]').remove();
					});

					$(document).on('click', '.bringBack', function (e) {
						e.preventDefault();
						var uid = $(this).data('uid');

						editor.destroy();
						var cache = $('.item[data-uid="' + uid + '"]');
						$('.item[data-uid="' + uid + '"]').remove();
						$('#page-viewer').prepend(cache);

						setTimeout(function () {
							editor.create();
						}, 500);
					});
					$(document).on('click', '.bringFront', function (e) {
						e.preventDefault();
						var uid = $(this).data('uid');

						editor.destroy();
						var cache = $('.item[data-uid="' + uid + '"]');
						$('.item[data-uid="' + uid + '"]').remove();
						$('#page-viewer').append(cache);

						setTimeout(function () {
							editor.create();
						}, 500);
					});

				// Text Toolbar

					// Negritas, italicas, subrayado
					$('[data-bold]').on('click', function (e) {
						e.preventDefault();
						var $self = $(this);
						var uid = $self.data('uid');

						if ($self.data('bold') == 'true') {
							$('.editable[data-uid="' + uid + '"]').css({ 'font-weight': 'normal' });
							$self.data('bold', 'false');
							$self.removeClass('btn-info').addClass('btn-default');
						} else {
							$('.editable[data-uid="' + uid + '"]').css({ 'font-weight': 'bold' });
							$self.data('bold', 'true');
							$self.removeClass('btn-default').addClass('btn-info');
						}
					});
					$('[data-italict]').on('click', function (e) {
						e.preventDefault();
						var $self = $(this);
						var uid = $self.data('uid');

						if ($self.data('italict') == 'true') {
							$('.editable[data-uid="' + uid + '"]').css({ 'font-style': 'normal' });
							$self.data('italict', 'false');
							$self.removeClass('btn-info').addClass('btn-default');
						} else {
							$('.editable[data-uid="' + uid + '"]').css({ 'font-style': 'italic' });
							$self.data('italict', 'true');
							$self.removeClass('btn-default').addClass('btn-info');
						}
					});
					$('[data-underline]').on('click', function (e) {
						e.preventDefault();
						var $self = $(this);
						var uid = $self.data('uid');

						if ($self.data('underline') == 'true') {
							$('.editable[data-uid="' + uid + '"]').css({ 'text-decoration': 'none' });
							$self.data('underline', 'false');
							$self.removeClass('btn-info').addClass('btn-default');
						} else {
							$('.editable[data-uid="' + uid + '"]').css({ 'text-decoration': 'underline' });
							$self.data('underline', 'true');
							$self.removeClass('btn-default').addClass('btn-info');
						}
					});

					// Align
					$('[data-align]').on('click', function (e) {
						e.preventDefault();
						var $self = $(this);
						var uid = $self.data('uid');

						$('[data-uid="' + uid + '"][data-align]').removeClass('btn-info');
						$('[data-uid="' + uid + '"][data-align="' + $self.data('align') + '"]').addClass('btn-info');

						$('.editable[data-uid="' + uid + '"]').css({ 'text-align': $self.data('align') });
					});

					// Font options
					$('[data-font]').on('click', function (e) {
						e.preventDefault();
						var $self = $(this);
						var uid = $self.data('uid');

						$('.editable[data-uid="' + uid + '"]').css({ 'font-family': $self.data('font') });
					});
					$('[data-size]').on('click', function (e) {
						e.preventDefault();
						var $self = $(this);
						var uid = $self.data('uid');

						$('.editable[data-uid="' + uid + '"]').css({ 'font-size': $self.data('size') });
						$('.sizeDisplay[data-uid="' + uid + '"]').text( $self.data('size') + ' px');
					});
					$('[data-color]').colorpicker().on('changeColor', function(e) {
						var uid = $(this).data('uid');

						$(this).children('i').css({ color: e.color.toHex() });
						$('.editable[data-uid="' + uid + '"]').css({ color: e.color.toHex() });
					});

			},
			destroy: function () {
				// Layer behaviors
					$('.resizable').resizable('destroy');
					$('.draggable').draggable('destroy');

				// Layer toolbar
					$(document).off('click', '.remove');
					$(document).off('click', '.bringBack');
					$(document).off('click', '.bringFront');

				// Text Toolbar
					$('[data-bold]').off('click');
					$('[data-italict]').off('click');
					$('[data-underline]').off('click');

					// Align
					$('[data-align]').off('click');

					// Font options
					$('[data-font]').off('click');
					$('[data-size]').off('click');
					$('[data-color]').colorpicker().off('changeColor');
			},

			save: function () {
				if (uid != '') {
					editor.destroy();

					$('#waiting').show();
					$('#page-viewer').css({ 'overflow': 'hidden' });


					$.each($('#page-viewer .item'), function (i, el) {
						var $el = $(el);
						var pageWidth = $('#page-viewer').width();
						var pageHeight = $('#page-viewer').height();

						// Scale

						var pwidth = utils.isPrecentOf(pageWidth, $el.outerWidth());
						var pheight = utils.isPrecentOf(pageHeight, $el.outerHeight());

						$el.data('pwidth', pwidth);
						$el.data('pheight', pheight);

						var nwidth = utils.isOfPercent(pageSize.w, pwidth);
						var nheight = utils.isOfPercent(pageSize.h, pheight);

						// Position

						var pleft = utils.isPrecentOf(pageWidth, $el.position().left);
						var ptop = utils.isPrecentOf(pageHeight, $el.position().top);

						$el.data('pleft', pleft);
						$el.data('ptop', ptop);

						var nleft = utils.isOfPercent(pageSize.w, pleft);
						var ntop = utils.isOfPercent(pageSize.h, ptop);

						$el.css({
							width: nwidth + 'px',
							height: nheight + 'px',

							left: nleft,
							top: ntop
						});
					});

					$.each($('.editable'), function(i, el) {
						var $el = $(el);
						var pageHeight = $('#page-viewer').height();

						var oFontSize = parseInt($el.css('font-size'));
						var pFontSize = utils.isPrecentOf(pageHeight, oFontSize);

						$el.data('pFontSize', pFontSize);

						var nFontSize = utils.isOfPercent(pageSize.h, pFontSize);
						$el.css({
							'font-size': Math.round(nFontSize) + 'px',
						});
					});



					$.post('/template/' + uid + '/update', {
						content: $('#page-viewer').html()
					}, function () {


						$.each($('#page-viewer .item'), function (i, el) {
							var $el = $(el);

							// Scale
							var pwidth = $el.data('pwidth');
							var pheight = $el.data('pheight');

							// Position

							var pleft = $el.data('pleft');
							var ptop = $el.data('ptop');

							$el.css({
								width: pwidth + '%',
								height: pheight + '%',

								left: pleft + '%',
								top: ptop + '%'
							});
						});

						$.each($('.editable'), function(i, el) {
							var $el = $(el);
							var pageHeight = $('#page-viewer').height();
							var pFontSize = $el.data('pFontSize');

							var nFontSize = utils.isOfPercent(pageHeight, pFontSize);
							$el.css({
								'font-size': Math.round(nFontSize) + 'px',
							});
						});


						var data = {}; data.message = '<i class="fa fa-save"></i> Se ha guardado el diseño de la plantilla';
						notifyOptions.type = 'success';
						$.notify(data, notifyOptions);

						setTimeout(function () {
							editor.create();
							$('#waiting').hide();
							$('#page-viewer').css({ 'overflow': 'initial' });
						}, 500)
					});
				}
			},

			resize: function () {
				var editorContainer = {
					height: $(window).height() - (
						$('#header-navbar').outerHeight() +
						$('#title-container').outerHeight() +
						$('#toolbar-container').outerHeight() +
						( parseInt( $('.content').css('padding-top') ) * 2) +
						( parseInt( $('.content').css('padding-bottom') ) * 2)
					)
				}
				$('#editor-container').height(editorContainer.height);
			},
			resizeOnce: function () {
				var editorContainer = {
					height: $(window).height() - (
						$('#header-navbar').outerHeight() +
						$('#title-container').outerHeight() +
						$('#toolbar-container').outerHeight() +
						( parseInt( $('.content').css('padding-top') ) * 2) +
						( parseInt( $('.content').css('padding-bottom') ) * 2)
					)
				}
				$('#editor-container').height(editorContainer.height);

				var pageHeight = editorContainer.height - (
						( parseInt( $('.content').css('padding-top') ) * 2) +
						( parseInt( $('.content').css('padding-bottom') ) * 2)
					);
				var pageWidth = utils.proporcional(210, 297, pageHeight);
				$('#page-viewer, #waiting').css({
					'height': pageHeight/.65,
					'width': pageWidth/.65,
					'margin-top': $('.content').css('padding-top'),
					'margin-bottom': $('.content').css('padding-bottom'),
					'margin-left': - (pageWidth / 1.3)
				});

				$.each($('#page-viewer .item'), function (i, el) {
					var $el = $(el);
					var pageWidth = $('#page-viewer').width();
					var pageHeight = $('#page-viewer').height();

					// Scale

					var pwidth = utils.isPrecentOf(pageSize.w, $el.outerWidth());
					var pheight = utils.isPrecentOf(pageSize.h, $el.outerHeight());

					$el.data('pwidth', pwidth);
					$el.data('pheight', pheight);

					var nwidth = utils.isOfPercent(pageWidth, pwidth);
					var nheight = utils.isOfPercent(pageHeight, pheight);

					// Position

					var pleft = utils.isPrecentOf(pageSize.w, $el.position().left);
					var ptop = utils.isPrecentOf(pageSize.h, $el.position().top);

					$el.data('pleft', pleft);
					$el.data('ptop', ptop);

					var nleft = utils.isOfPercent(pageWidth, pleft);
					var ntop = utils.isOfPercent(pageHeight, ptop);

					$el.css({
						width: nwidth + 'px',
						height: nheight + 'px',

						left: nleft + 'px',
						top: ntop + 'px'
					});
				});

				$.each($('.editable'), function(i, el) {
					var $el = $(el);
					var pageHeight = $('#page-viewer').height();

					var oFontSize = parseInt($el.css('font-size'));
					var pFontSize = utils.isPrecentOf(pageSize.h, oFontSize);

					$el.data('pFontSize', pFontSize);

					var nFontSize = utils.isOfPercent(pageHeight, pFontSize);
					$el.css({
						'font-size': Math.round(nFontSize) + 'px',
					});
				});
			},

			events: function () {
				$(window).resize(editor.resize);

				// Quitar foco a los elementos cuando no se presionan
					$('.nocanvas').on('click', function () {
						$('.item').removeClass('focus');
					});

				// Añadir texto
					$(document).on('click', '.addtext', function (e) {
						e.preventDefault();
						var guid = utils.guid();

						editor.destroy();
						var content = $(this).data('content');

						var renderText = Mustache.render(tpl_text, { uid: guid, text: content });
						var renderItemText = Mustache.render(tpl_item_text, { uid: guid, content: renderText, type: 'text' });
						$('#page-viewer')
							.append(renderItemText);

						setTimeout(function () {
							editor.create();
						}, 500);
					});

				// Añadir firma
					$(document).on('click', '[data-add="sign"]', function (e) {
						e.preventDefault();
						var guid = utils.guid();

						editor.destroy();
						var picture = $(this).data('picture');

						var renderImg = Mustache.render(tpl_src, { uid: guid, src: '{{ asset("/pictures") }}'+'/'+picture });
						var renderItem = Mustache.render(tpl_item, { uid: guid, content: renderImg, type: 'img', width: $(this).data('width'), height: $(this).data('height') });

						console.log(guid);
						console.log(picture);
						console.log(renderImg);
						console.log(renderItem);

						$('#page-viewer')
							.append(renderItem);

						setTimeout(function () {
							editor.create();
						}, 500);
					});

				// Añadir imagen
					$(document).on('click', '[data-add="picture"]', function (e) {
						e.preventDefault();
						var guid = utils.guid();

						editor.destroy();
						var picture = $(this).data('picture');
						
						var renderImg = Mustache.render(tpl_src, { uid: guid, src: picture });
						var renderItem = Mustache.render(tpl_item, { uid: guid, content: renderImg, type: 'img', width: $(this).data('width'), height: $(this).data('height') });
						$('#page-viewer')
							.append(renderItem);

						$('#modal-choosePicture').modal('hide');

						setTimeout(function () {
							editor.create();
						}, 500);
					});

				// Abrirl modal de imagenes
					$(document).on('click', '.choose-picture', function (e) {
						e.preventDefault();
						$('#pictures-container').empty();

						$.get('{{route('pictures.list')}}', function (res) {
							$.each(res.data, function (i, pic) {
								var renderPicture = Mustache.render(tpl_picture, { picture: pic.url, width: pic.width, height: pic.height });
								$('#pictures-container').append(renderPicture);
							});
							$('#modal-choosePicture').modal('show');
						});

					});

				// Vaciar canvas
					$(document).on('click', '.restart', function (e) {
						$('#page-viewer').empty();
					});

				// Guardar
					$(document).on('click', '.save', function (e) {
						editor.save();
					});

				// Guardar y finalizar
					$(document).on('click', '.finish', function (e) {
						editor.save();
						utils.redirect('{{route('documents.view', ['uid' => $document->uid])}}');
					});
			},
			init: function () {
				editor.events();
				editor.resizeOnce();
				editor.create();

				if ( {{$document->template == '' ? 'true' : 'false'}} ) {
					$.post('{{route('template.create')}}', {
						document: '{{$document->uid}}',
						content: $('#page-viewer').html()
					}, function (res) {
						uid = res.template.uid;
					});
				}
			}
		}
		$(editor.init);
	</script>
@stop

@section('content')
	<script id="tpl_item" type="text/html">
		<div class="item resizable draggable" data-uid="@{{ uid }}" data-type="@{{ type }}" data-width="@{{ width }}" data-height="@{{ height }}" data-firstsize="yep">
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>

			<div class="upperright">
				<!-- Layer options -->
				<div class="btn-group">
					<button class="btn btn-xs btn-default bringBack" data-uid="@{{ uid }}"><i class="fa fa-toggle-down"></i></button>
					<button class="btn btn-xs btn-default bringFront" data-uid="@{{ uid }}"><i class="fa fa-toggle-up"></i></button>
					<button class="btn btn-xs btn-default remove" data-uid="@{{ uid }}"><i class="fa fa-times"></i></button>
				</div>
			</div>
			@{{{ content }}}
		</div>
	</script>

	<script id="tpl_item_text" type="text/html">
		<div class="item resizable draggable" data-uid="@{{ uid }}" data-type="@{{ type }}">

			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>
			<div class="anchor"></div>

			<button class="btn btn-xs btn-default movee"><i class="fa fa-arrows"></i></button>

			<div class="upperright">

				<!-- Negritas, italicas, subrayado -->
				<div class="btn-group">
					<button class="btn btn-xs btn-default" data-uid="@{{ uid }}" data-bold="false"><i class="fa fa-bold"></i></button>
					<button class="btn btn-xs btn-default" data-uid="@{{ uid }}" data-italict="false"><i class="fa fa-italic"></i></button>
					<button class="btn btn-xs btn-default" data-uid="@{{ uid }}" data-underline="false"><i class="fa fa-underline"></i></button>
				</div>

				<!-- Font options -->
				<div class="btn-group">

					<!-- Font family -->
					<div class="btn-group">
						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-font"></i>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#" data-uid="@{{ uid }}" data-font="helvetica">Helvetica</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-font="arial">Arial</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-font="Open Sans">Open Sans</a></li>
						</ul>
					</div>

					<!-- Font size -->
					<div class="btn-group">
						<button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
							<span data-uid="@{{ uid }}" class="sizeDisplay">24 px</span>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#" data-uid="@{{ uid }}" data-size="8">8 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="10">10 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="12">12 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="14">14 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="16">16 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="18">18 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="20">20 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="22">22 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="24">24 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="26">26 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="28">28 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="30">30 px</a></li>
							<li><a href="#" data-uid="@{{ uid }}" data-size="32">32 px</a></li>
						</ul>
					</div>

					<!-- Font color -->
					<button class="btn btn-default btn-xs addtext" data-uid="@{{ uid }}" data-color="#646464">
						<i class="fa fa-square" style="color: #646464;"></i>
					</button>
				</div>

				<!-- Align -->
				<div class="btn-group">
					<button class="btn btn-xs btn-default btn-info" data-uid="@{{ uid }}" data-align="left"><i class="fa fa-align-left"></i></button>
					<button class="btn btn-xs btn-default" data-uid="@{{ uid }}" data-align="center"><i class="fa fa-align-center"></i></button>
					<button class="btn btn-xs btn-default" data-uid="@{{ uid }}" data-align="right"><i class="fa fa-align-right"></i></button>
				</div>

				<!-- Layer options -->
				<div class="btn-group">
					<button class="btn btn-xs btn-default bringBack" data-uid="@{{ uid }}"><i class="fa fa-toggle-down"></i></button>
					<button class="btn btn-xs btn-default bringFront" data-uid="@{{ uid }}"><i class="fa fa-toggle-up"></i></button>
					<button class="btn btn-xs btn-default remove" data-uid="@{{ uid }}"><i class="fa fa-times"></i></button>
				</div>
			</div>
			@{{{ content }}}
		</div>
	</script>

	<script id="tpl_text" type="text/html">
		<div class="editable" data-uid="@{{ uid }}" contenteditable>@{{ text }}</div>
	</script>
	<script id="tpl_src" type="text/html">
		<img src="@{{ src }}" />
	</script>
	<script id="tpl_sign" type="text/html">
		<div class="sign-container">
			<img src="{{URL::asset('/')}}pictures/@{{picture}}" />
		</div>
	</script>

	<script id="tpl_picture" type="text/html">
		<div class="col-sm-6 col-md-4 col-lg-3 push-20 animated fadeIn">
			<div class="img-container" style="height: 140px; background-color: #444; background-image: url('{{URL::asset('/')}}pictures/thumb//@{{picture}}'); background-size: cover; background-position: center;">
				<div class="img-options">
					<div class="img-options-content">
						<button class="btn btn-sm btn-default trash-picture" data-add="picture" data-picture="{{URL::asset('/')}}pictures/@{{picture}}" data-width="@{{width}}" data-height="@{{height}}">Seleccionar</button>
					</div>
				</div>
			</div>
		</div>
	</script>




	<div id="toolbar-container" class="content">
		<div class="pull-right">
			<button class="btn btn-default restart">
				<i class="fa fa-undo"></i>
				<span>Comenzar de nuevo</span>
			</button>
			<button class="btn btn-info save">
				<i class="fa fa-save"></i>
				<span>Guardar</span>
			</button>
			<button class="btn btn-success finish">
				<i class="fa fa-check-circle-o"></i>
				<span>Finalizar</span>
			</button>
		</div>
		<div>
			<div class="btn-group">
				<button class="btn btn-default addtext" data-content="- Inserta texto -">
					<i class="fa fa-paragraph"></i>
					<span>Texto</span>
				</button>
				<div class="btn-group">
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Documento</li>
						<li><a href="#" class="addtext" data-content="[document.name]">Nombre</a></li>
						<li><a href="#" class="addtext" data-content="[document.description]">Descripción</a></li>
						<li><a href="#" class="addtext" data-content="[document.folio]">Folio</a></li>
						<li><a href="#" class="addtext" data-content="[document.date]">Fecha</a></li>
					<li class="divider" class="addtext" data-content=""></li>
						<li class="dropdown-header">Persona</li>
						<li><a href="#" class="addtext" data-content="[person.rut]">RUT</a></li>
						<li><a href="#" class="addtext" data-content="[person.name]">Nombre</a></li>
						<li><a href="#" class="addtext" data-content="[person.email]">Email</a></li>
						<li><a href="#" class="addtext" data-content="[person.anexo1]">Anexo 1</a></li>
						<li><a href="#" class="addtext" data-content="[person.anexo2]">Anexo 2</a></li>
						<li><a href="#" class="addtext" data-content="[person.anexo3]">Anexo 3</a></li>
					</ul>
				</div>
			</div>
			<div class="btn-group">
				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-image"></i>
					<span>Imagen</span>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#" class="add-picture">Subir imagen</a></li>
					<li><a href="#" class="choose-picture">Seleccionar imagen existente</a></li>
				</ul>
			</div>
			<div class="btn-group">
				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-pencil-square-o"></i>
					<span>Firmas</span>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					@forelse($document->osigns() as $s)
						<li><a href="#" data-add="sign" data-name="{{$s->name}}" data-title="{{$s->title}}" data-picture="{{$s->picture->url}}" data-width="{{$s->picture->width}}" data-height="{{$s->picture->height}}">{{$s->name}}</a></li>
					@empty
						<li><a href="#">Sin firmas</a></li>
					@endforelse
				</ul>
			</div>
		</div>
	</div>




	<div class="content">
		<div class="row">
			<div class="col-lg-12">

				<div id="editor-container">
					<div id="page-viewer">
						<div class="nocanvas"></div>
						{!! $template->layout !!}


						<div id="guide-h" class="guide"></div>
						<div id="guide-v" class="guide"></div>
					</div>




					<div id="waiting" class="text-center text-white-op" style="display: none;">
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<h1 class="h2 font-w300">Guardando cambios</h1>
						<br>
						<i class="fa fa-cog fa-spin fa-2x"></i>
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection


@section('modals')
	<div class="modal" id="modal-addPicture" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Subir imagen</h3>
					</div>
					<div class="block-content block-content-full">

							<div class="form-group pull-t pull-r-l pull-b">
								<div id="img_pic" class="img-container" style="height: 150px; background: #444 center no-repeat; background-size: cover;">
									<div class="img-options" style="opacity: 1;">
										<div class="img-options-content">
											<h4 class="h6 font-w400 text-white-op push-15">Selecciona una imagen</h4>
											<button type="button" id="picker_pic" class="btn btn-sm btn-default"><i class="si si-magnifier"></i> Buscar imagen</a>
										</div>
									</div>
								</div>

								<div id="progress_pic" class="progress progress-mini animated fadeIn slideInDown" style="display: none;">
									<div id="progressbar_pic" class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
								</div>

								<input type="file" id="file_pic" name="file_pic" style="display: none;" />

								<div id="error_pic" class="alert alert-danger alert-dismissable" style="display: none;">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<h3 class="font-w300 push-15">Error</h3>
									<p id="error_txt_pic"></p>
								</div>
							</div>

					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modal-choosePicture" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Seleccionar imagen</h3>
					</div>
					<div class="block-content block-content-full">

						<div class="row items-push" id="pictures-container"></div>

					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
@stop

