@extends('template')

@section('breadcrumb')
	<li>Inicio</li>
	<li><a href="{{route('signs.index')}}">Firmas</a></li>
	<li class="active">Nueva</li>
@stop

@section('scripts')
	<script src="{{URL::asset('/oneui/js/dnn.upload.js')}}"></script>
	<script>
	// Upload
	var pictureAPI = "{{route('pictures.upload')}}";

	var options_sign = {
	    url: pictureAPI,
	    filename: 'file',
	    group: 'Signs',
	    maxSize: 8 * 1024 * 1024,
	    maxWidth: 720,
	    start: function () {
	    },
	    process: function () {
	    },
	    error: function (error) {
	        console.log(error.message);
	    },
	    xhr: function () {
	        var xhr = new window.XMLHttpRequest();
	        return xhr;
	    },
	    success: function (response) {
	        $('#pic_sign').val(response.id);
	        $('#img_sign').attr('src', response.pic).fadeIn();
	    }
	};

	$(function () {
	    // Logo
	    var input_sign = $('#file_sign');

	    $('#explore_sign').click(function (e) {
	    	e.preventDefault();
	    	input_sign.trigger('click');
	    })

	    input_sign.on('change', function (e) {
	        e.preventDefault();
	        options_sign.files = this.files;
	        upload( options_sign );
	    });
	});

	@if($errors->has())
	$(function () {
		$('#modal-errors').modal('show');
	});
	@endif

	</script>
@stop

@section('content')
	<div class="content">
		<!-- Formulario -->
		
		<form action="{{route('signs.create')}}" method="POST">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-lg-4 col-sm-6 col-xs-12 col-lg-offset-4 col-sm-offset-3">
				<div class="block">
					<div class="block-content">

						<div class="form-group" style="margin-bottom: 20px; padding: 20px 0;">
							<label>Imagen de la firma</label>
							<div class="media" style="border: 1px solid #eee;">
								<div class="text-center" style="padding: 20px; min-height: 48px;">
									<img id="img_sign" src="#" style="width: 100%; display: none;" />
								</div>
							</div>
							<div style="display: none;">
								<input type="hidden" id="pic_sign" name="pic_sign" />
								<input type="file" id="file_sign" name="file_sign" />
							</div>
							<button id="explore_sign" class="btn btn-default btn-block"><i class="fa fa-search"></i> Seleccionar firma</button>
						</div>

						<div class="form-group">
							<div class="form-material">
								<input class="form-control input-lg" type="text" name="name" placeholder="Nombre" value="{{old('name')}}">
							</div>
						</div>
						<div class="form-group">
							<div class="form-material">
								<input class="form-control" type="text" name="title" placeholder="Título" value="{{old('title')}}">
							</div>
						</div>

						<div class="form-group" style="border-top: 1px solid #eee; margin-top: 20px; padding-top: 20px;">
							<button class="btn btn-primary" type="submit">Añadir</button>
							<a href="{{route('signs.index')}}" class="btn btn-default" role="button">Cancelar</a>
						</div>

					</div>
				</div>
			</div>
		</div>
		</form>

	</div>
@stop

@section('modals')

	@if($errors->has())
		<?php $dis = '' ?>
		@foreach ($errors->all() as $error)
			<?php $dis .= "<li>{$error}</li>" ?>
		@endforeach

		<div class="modal" id="modal-errors" tabindex="-1" role="dialog" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="block block-themed remove-margin-b">
		                <div class="block-header bg-danger">
		                    <ul class="block-options">
		                        <li>
		                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
		                        </li>
		                    </ul>
		                    <h3 class="block-title">Verfica lo siguiente</h3>
		                </div>
		                <div class="block-content">
		                	<ul>
								{{$dis}}
							</ul>
						</div>
		            </div>
		            <div class="modal-footer">
		                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal">Aceptar</button>
		            </div>
		        </div>
		    </div>
		</div>
	@endif
@stop