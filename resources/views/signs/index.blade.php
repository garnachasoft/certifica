@extends('template')

@section('breadcrumb')
        <li>Inicio</li>
        <li class="active">Firmas</li>
@stop
@section('content')
	<div class="content">
		<div class="btn-group">
			<a href="{{route('signs.new')}}" class="btn btn-default" role="button">Nueva firma</a>
		</div>
	</div>

	<div class="content">
		<div class="row">
		@forelse($signs as $s)

			<div class="col-sm-6 col-md-4 col-lg-3">
				<a class="block block-link-hover3 block-bordered" href="{{route('signs.edit', ['uid' => $s->uid])}}">
					<div class="block-content block-content-full text-center">

						<img src="{{URL::asset('/pictures/' . $s->picture->url)}}" style="width: 100%;" />

					</div>
					<div class="block-content bg-gray-lighter">

						<h1 class="h4">{{$s->name}}</h1>
						<p>{{$s->title}}</p>

					</div>
				</a>
			</div>
			
		@empty

            <div class="col-lg-12">
                <a class="block block-link-hover3" href="{{route('signs.new')}}">
                    <div class="block-content block-content-full clearfix text-center">
                        <h1 class="h4 push-20-t">Aún no hay ninguna firma</h1>
                        <p>Crea tu primer frima picando aquí</p>
                    </div>
                </a>
            </div>

		@endforelse
		</div>
	</div>
@endsection


