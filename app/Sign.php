<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sign extends Model
{
	protected $table = 'signs';

    public function picture () {
        return $this->hasOne('App\Picture', 'uid', 'handwrite');
    }
}
