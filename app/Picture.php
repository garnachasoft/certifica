<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{

	protected $table = 'pictures';

	public function dependences () {
		$users = $this->hasMany('App\User', 'avatar', 'uid');
		$signs = $this->hasMany('App\Sign', 'handwrite', 'uid');

		$md5 = $this->md5;
		$templates = Template::where('layout', 'LIKE', "%{$md5}%");

		$coutn = 0
			+ $users->count()
			+ $signs->count()
			+ $templates->count();

		return $coutn;
	}

}
