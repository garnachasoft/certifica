<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if (!Auth::check()) {
    	return redirect('/login');
    } else {
        return redirect('/documents');
    }
});
Route::auth();

// Prevent dfault routes
Route::any('/register', function () {
    return redirect('/');
});

// Search
Route::get('/search', ['as' => 'search.index', 'uses' => 'SearchController@viewDashboard']);
Route::get('/search/by/rut', ['middleware' => 'cors', 'as' => 'search.by.rut', 'uses' => 'SearchController@searchByRut']);

// Pictures
Route::get('/images', ['as' => 'pictures.index', 'uses'=>'PictureController@viewIndex']);
Route::get('/picture/uid/{uid}', ['as' => 'pictures.uid', 'uses'=>'PictureController@uid']);
Route::post('/picture/upload', ['as' => 'pictures.upload', 'uses'=>'PictureController@upload']);
Route::post('/picture/delete', ['as' => 'pictures.delete', 'uses'=>'PictureController@delete']);
Route::get('/picture/list', ['as' => 'pictures.list', 'uses'=>'PictureController@listjson']);

// Documents
Route::get('/documents', ['as' => 'documents.index', 'uses' => 'DocumentController@viewIndex']);
Route::get('/document/new', ['as' => 'documents.new', 'uses' => 'DocumentController@viewNew']);
Route::get('/document/{uid}', ['as' => 'documents.view', 'uses' => 'DocumentController@viewView']);
Route::get('/document/{uid}/edit', ['as' => 'documents.edit', 'uses' => 'DocumentController@viewEdit']);

Route::post('/document/create', ['as' => 'documents.create', 'uses' => 'DocumentController@actionCreate']);
Route::post('/document/{uid}/update', ['as' => 'documents.update', 'uses' => 'DocumentController@actionUpdate']);
Route::get('/document/{uid}/delete', ['as' => 'documents.delete', 'uses' => 'DocumentController@actionDelete']);

Route::post('/document/{uid}/notify/all', ['as' => 'documents.notify.all', 'uses' => 'DocumentController@notifyAll']);
Route::post('/document/{uid}/notify/one/{one}', ['as' => 'documents.notify.one', 'uses' => 'DocumentController@notifyOne']);

// Certificates
Route::post('/certificates/import', ['as' => 'certificates.import', 'uses' => 'CertificateController@actionImport']);
Route::get('/certificates/{uid}/download', ['as' => 'certificates.download', 'uses' => 'CertificateController@actionDownload']);
Route::get('/certificates/{uid}/edit', ['middleware' => 'auth', 'as' => 'certificates.edit', 'uses' => 'CertificateController@edit']);
Route::post('/certificates/{uid}/update', ['middleware' => 'auth', 'as' => 'certificates.update', 'uses' => 'CertificateController@update']);
Route::get('/certificates/{uid}/delete', ['middleware' => 'auth', 'as' => 'certificates.delete', 'uses' => 'CertificateController@delete']);
Route::post('/certificates/delete/massive', ['middleware' => 'auth', 'as' => 'certificates.massive.delete', 'uses' => 'CertificateController@massiveDelete']);

// Templates
Route::get('/document/{uid}/template/editor', ['as' => 'documents.template.editor', 'uses' => 'TemplateController@viewEditor']);
Route::get('/document/{uid}/template/preview', ['as' => 'documents.template.preview', 'uses' => 'TemplateController@viewPreview']);
Route::get('/document/{uid}/template/printable', ['as' => 'documents.template.printable', 'uses' => 'TemplateController@viewPrintable']);

Route::post('/template/create', ['as' => 'template.create', 'uses' => 'TemplateController@actionCreate']);
Route::post('/template/{uid}/update', ['as' => 'template.update', 'uses' => 'TemplateController@actionUpdate']);
Route::get('/template/{uid}/delete', ['as' => 'template.delete', 'uses' => 'TemplateController@actionDelete']);

// Signs
Route::get('/signs', ['as' => 'signs.index', 'uses' => 'SignController@viewIndex']);
Route::get('/sign/new', ['as' => 'signs.new', 'uses' => 'SignController@viewNew']);
Route::get('/sign/{uid}/edit', ['as' => 'signs.edit', 'uses' => 'SignController@viewEdit']);

Route::post('/sign/create', ['as' => 'signs.create', 'uses' => 'SignController@actionCreate']);
Route::post('/sign/{uid}/update', ['as' => 'signs.update', 'uses' => 'SignController@actionUpdate']);
Route::get('/sign/{uid}/delete', ['as' => 'signs.delete', 'uses' => 'SignController@actionDelete']);

// Users
Route::get('/users', ['as' => 'users.index', 'uses' => 'UserController@viewIndex']);
Route::get('/user/new', ['as' => 'users.new', 'uses' => 'UserController@viewNew']);
Route::get('/user/{uid}/edit', ['as' => 'users.edit', 'uses' => 'UserController@viewEdit']);

Route::post('/user/{uid}/update', ['as' => 'users.update', 'uses' => 'UserController@actionUpdate']);
Route::get('/user/{uid}/delete', ['as' => 'users.delete', 'uses' => 'UserController@actionDelete']);
Route::post('/user/create-user', ['as' => 'users.create', 'uses' => 'UserController@actionCreate']);