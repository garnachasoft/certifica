<?php

namespace App\Http\Controllers;

use Mail;
use App\Document;
use App\Sign;
use App\Certificate;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DocumentController extends Controller {

    // NPI: Supongo que esto te permite acceder cuando tengas login
    public function __construct() {
        $this->middleware('auth');
    }

    // Views
    public function viewIndex () {
        $documents = Document::all();
        $data = array (
            'title' => 'Documentos',
            'section' => 'documents',
            'documents' => $documents
        );
        return view('documents.index', $data);
    }

    public function viewNew () {
        $signs = Sign::all();
        $data = array (
            'title' => 'Nuevo documento',
            'section' => 'documents',
            'signs' => $signs
        );
        return view('documents.new', $data);
    }

    public function viewEdit ($uid) {
        $documents = Document::where('uid', '=', $uid)->take(1)->get();
        $document = $documents[0];
        $signs = Sign::all();

        $data = array (
            'title' => 'Documento',
            'section' => 'documents',
            'document' => $document,
            'signs' => $signs
        );
        return view('documents.edit', $data);
    }

    public function viewView ($uid) {
        $documents = Document::where('uid', '=', $uid)->take(1)->get();
        $document = $documents[0];

        $certificates = Certificate::where('document', '=', $uid)->orderBy('created_at', 'desc')->paginate(12);

        $data = array (
            'title' => $document->name,
            'section' => 'documents',
            'document' => $document,
            'certificates' => $certificates
        );
        return view('documents.view', $data);
    }

    // Actions
    public function actionCreate (Request $request) {
        $rules = array(
            'name' => 'required'
        );

        $messages = array (
            'name.required' => 'El nombre es necesario',
        );

        //check validation
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('documents.create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $document = new Document;
            $document->uid = uniqid();
            $document->name = $request->input('name');
            $document->description = $request->input('description');
            $document->notes = $request->input('notes');
            $document->signs = $request->input('signs');
            $document->save();

            return redirect()->route('documents.index');
        }
    }
    public function actionUpdate (Request $request, $uid) {
        $rules = array(
            'name' => 'required'
        );

        $messages = array (
            'name.required' => 'El nombre es necesario',
        );

        //check validation
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('documents.create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $documents = Document::where('uid', '=', $uid)->take(1)->get();
            $document = $documents[0];

            $document->name = $request->input('name');
            $document->description = $request->input('description');
            $document->notes = $request->input('notes');
            $document->signs = $request->input('signs');
            $document->save();

            return redirect()->route('documents.view', ['uid' => $document->uid]);
        }
    }
    public function actionDelete ($uid) {
        $documents = Document::where('uid', '=', $uid)->take(1)->get();
        $document = $documents[0];
        $document->delete();
        return redirect()->route('documents.index');
    }

    public function notifyAll ($uid) {
        $certificates = Certificate::where('document', '=', $uid)->get();
        $certificates = $certificates->each(function ($item, $key) {
            if ($item->email != '') {
                Mail::send('emails.certificado', ['person' => $item], function ($m) use ($item) {
                    $m->from('no-reply@app.com', 'Certifica');
                    $m->to($item->email, $item->nombre)->subject('Certificado');
                });
            }
        });

        return response()->json([
            'status' => '200',
        ]);
    }
    public function notifyOne ($uid, $one) {
        $certificates = Certificate::where([
                ['document', '=', $uid],
                ['uid', '=', $one],
            ])->get();
        $item = $certificates[0];

        if ($item->email != '') {
            Mail::send('emails.certificado', ['person' => $item], function ($m) use ($item) {
                $m->from('no-reply@app.com', 'Certifica');
                $m->to($item->email, $item->nombre)->subject('Certificado');
            });
        }

        return response()->json([
            'status' => '200',
        ]);
    }
    
    // Api
}