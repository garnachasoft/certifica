<?php

namespace App\Http\Controllers;

use App\Certificate;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller {

    // Views
    public function viewPublic () {
    }
    public function viewDashboard () {
        $data = array (
            'title' => 'Buscar',
            'section' => 'search',
        );
        return view('search.dashboard', $data);
    }

    // Actions

    public function searchByRut (Request $request) {
        $certificates = Certificate::where('rut', 'LIKE', '%' . $request->input('rut') . '%')
                                    ->orWhere('uid', 'LIKE', '%' . $request->input('rut') . '%')
                                    ->get();

        return response()->json([
            'status' => '200',
            'data' => $certificates
        ]);
    }
}