<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller {

    // NPI: Supongo que esto te permite acceder cuando tengas login
    public function __construct() {
        $this->middleware('auth');
    }

    // Views
    public function viewIndex () {
        $users = User::orderBy('name', 'asc')->get();
        $data = array(
            'title' => 'Usuarios',
            'section' => 'users',
            'users' => $users
        );
        return view('users.index', $data);
    }

    public function viewNew () {
        $data = array(
            'title' => 'Nuevo usuario',
            'section' => 'users'
        );
        return view('users.new', $data);
    }

    public function viewEdit ($uid) {
        $user = User::where('uid', '=', $uid)->take(1)->get();
        $data = array(
            'title' => "Editar usuario",
            'section' => 'users',
            'user' => $user[0]
        );
        return view('users.edit', $data);
    }

    // Actions
    public function actionCreate (Request $request) {
        $rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        );

        $messages = array (
            'name.required' => 'El nombre es necesario',
            'email.required' => 'El email es obligatorio',
            'email.email' => 'No colocaste un email válido',
            'password.required' => 'Es obligatorio colocar una contraseña'
        );

        //check validation
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('users.new')
                ->withErrors($validator)
                ->withInput();
        } else {
            $user = new User;
            $user->uid =  uniqid();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));

            $pic_avatar = $request->input('pic_avatar');
            $pic_avatar = !empty($pic_avatar) ? $pic_avatar : 'avatar';
            $user->avatar = $pic_avatar;

            $user->save();

            return redirect()->route('users.index');
        }
    }

    public function actionUpdate (Request $request, $uid) {   //validation videos
        $rules = array(
            'name' => 'required',
            'email' => 'required|email',
        );
        $messages = array(
            'name.required' => 'El nombre es necesario',
            'email.required' => 'El email es obligatorio',
            'email.email' => 'No colocaste un email válido'
        );

        //check validation
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('users.edit', ['id'=>$id])
                ->withErrors($validator)
                ->withInput();
        } else {
            $users = User::where('uid', '=', $uid)->take(1)->get();
            $user = $users[0];

            $user->name = $request->input('name');
            $user->email = $request->input('email');

            if ($request->has('password')) {
                $user->password = bcrypt($request->input('password'));
            }

            $pic_avatar = $request->input('pic_avatar');
            $pic_avatar = !empty($pic_avatar) ? $pic_avatar : 'avatar';
            $user->avatar = $pic_avatar;

            $user->save();
            return redirect()->route('users.edit', ['uid' => $user->uid]);
        }
    }
    public function actionDelete ($uid) {
        $users = User::where('uid', '=', $uid)->take(1)->get();
        $user = $users[0];
        $user->delete();
        return redirect()->route('users.index');
    }
    
    // Api
}