<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Auth;

class AuthController extends Controller{

    public function login(){
        $data = array(
            'title' => 'Entrar',
        );

        return view('login.login', $data);
    }

    public function dologin(){
        $email = $request->input('email');
        $password = $request->input('password');
        if ( Auth::attempt(['email' => $email, 'password' => $password]) ) {
            return redirect()->route('home');
        }else{
            return back()->with('error', 'El email o la contraseña son incorrectos');
        }
    }

    public function logout(){
    	Auth::logout();
    }
}
