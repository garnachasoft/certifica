<?php

namespace App\Http\Controllers;

use File;
use App\Picture;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Intervention\Image\ImageManagerStatic as Image;

class PictureController extends Controller {

    // NPI: Supongo que esto te permite acceder cuando tengas login

    // Views

    public function viewIndex () {
        $pictures = Picture::orderBy('created_at', 'desc')->paginate(12);
        $data = array(
            'title' => 'Imágenes',
            'section' => 'pictures',
            'pictures' => $pictures
        );
        return view('pictures.index', $data);
    }

    // Actions

    // API

    public function upload (Request $request) {
        set_time_limit(3600);
        $up = $request->hasFile('file');
        $status = array();

        $group = $request->input('group');
        $group = isset($group) ? $group : "General";
        if ($up) {
            // guardamos la imágen en una variabñe
            $image = $request->file('file');

            // obtenemos el md5
            $md5 = md5_file($image);

            // consultamos el md5 en la bd
            $imagen = Picture::whereMd5($md5)->get();

            // si no encontramos coincidencias subimos
            if ($imagen->isEmpty()) {
                //traemos la extensión
                $ext = $image->getClientOriginalExtension();

                //generamos un uid
                $uid = uniqid();

                // generamos el nombre de la imagen
                $filename = $md5 . '.' . $ext;

                // se mueve la imágen a la carpeta pictures
                $image->move('pictures', $filename);
                $fileUrl = asset('pictures/' . $filename);
                $fileUrlMiddle = asset('pictures/' . $filename);
                $sqm = asset('pictures/sqm/' . $filename);

                // asignamos las carpetas a variables
                $file = public_path('pictures/' . $filename);
                $pathSqm = public_path('pictures/sqm/' . $filename);
                $pathThumb = public_path('pictures/thumb/' . $filename);

                // almacenamos la imagen original
                $opicture = Image::make($file);

                // redimensiones, a todos tamaños  de carpetascuidando el upsize
                Image::make($file)->resize(160, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($pathThumb);
                Image::make($file)->resize(64, 64)->save($pathSqm);

                //insertamos la imagen en la bd
                $picture = new Picture;
                $picture->uid = $uid;
                $picture->md5 = $md5;
                $picture->url = $filename;
                $picture->group = $group;

                $picture->content_type = $opicture->mime();
                $picture->width = $opicture->width();
                $picture->height = $opicture->height();
                $picture->exif = json_encode($opicture->exif());
                $picture->save();

                //guardamos el status
                $status = array(
                    'status' => 'success',
                    'time'=> array(
                        'time' => time()
                    ),
                    'description' => 'Se guardó la imagen',
                    'pic' => $fileUrlMiddle,
                    'url' => $filename,
                    'filelink' => $fileUrlMiddle,
                    'sqm' => $sqm,
                    'id' => $uid,
                    'width' => $picture->width,
                    'height' => $picture->height
                );
            } else {
                // si la imágen ya existe obtenemos la url
                $fileUrlMiddle = asset('pictures/' . $imagen[0]->url);
                $sqm = asset('pictures/sqm/' . $imagen[0]->url);

                // guardamos el status en json
                $status = array(
                    'status' => 'repeat',
                    'time'=> array(
                        'time' => time()
                    ),
                    'description' => 'La imágen ya existe',
                    'pic' => $fileUrlMiddle,
                    'url' => $imagen[0]->url,
                    'filelink' => $fileUrlMiddle,
                    'sqm' => $sqm,
                    'id' => $imagen[0]->uid,
                    'width' => $imagen[0]->width,
                    'height' => $imagen[0]->height
                );
            }
        } else {
            $status = array(
                'status' => 'error',
                'time'=> array(
                    'time' => time()
                ),
                'error' => 'error',
                'pic' => 'error'
            );
        }
        return response()->json($status);

    }

    public function uid ($uid) {
        $pic = Picture::where('uid', '=', $uid)->take(1)->get();
        return redirect(asset('/pictures/' . $pic[0]->url));
    }

    public function delete (Request $request) {
        $uid = $request->input('uid');
        $pictures = Picture::where('uid', '=', $uid)->take(1)->get();
        $picture = $pictures[0];

        if ($picture->dependences() > 0) {
            $status = array (
                'status' => 'error',
                'time'=> array(
                    'time' => time()
                ),
                'error' => 'Esta imagen tiene objetos que dependen de ella'
            );
            return response()->json($status);
        } else {
            $filename = $picture->url;
            $files = array (
                public_path('pictures/' . $filename),
                public_path('pictures/sqm/' . $filename),
                public_path('pictures/thumb/' . $filename)
            );
            File::delete($files);

            $picture->delete();
            $status = array (
                'status' => 'success',
                'time'=> array(
                    'time' => time()
                )
            );
            return response()->json($status);
        }
    }


    public function listjson ($group = 'all') {
        $pictures = Picture::all();
        $res = array(
            'status' => 200,
            'data' => $pictures
        );
        return response()->json($res);
    }

}