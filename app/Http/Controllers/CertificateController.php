<?php

namespace App\Http\Controllers;

use File;
use App\Certificate;
use App\Document;
use App\Template;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Excel;
//use Dompdf\Dompdf;
use PDF;

class CertificateController extends Controller {

    // Actions

    public function actionImport (Request $request) {
        $up = $request->hasFile('excel_import');
        $document = $request->input('document');
        $status = array();
        if ($up) {
            Excel::load( $request->file('excel_import'), function ($reader) use ($document) {
                $reader->each(function ($sheet) use ($document) {

                    $certificate = new Certificate;
                    $certificate->uid =  uniqid();

                    $certificate->document = $document;

                    $certificate->rut = $sheet->rut;
                    $certificate->nombre = $sheet->nombre;
                    $certificate->email = $sheet->email;
                    $certificate->anexo1 = $sheet->anexo1;
                    $certificate->anexo2 = $sheet->anexo2;
                    $certificate->anexo3 = $sheet->anexo3;

                    $certificate->save();

                });
            });
        }
        return redirect()->route('documents.view', ['uid' => $document]);
    }

    public function actionDownload ($uid) {
        $certificates = Certificate::where('uid', '=', $uid)->take(1)->get();
        $certificate = $certificates[0];

        $documents = Document::where('uid', '=', $certificate->document)->take(1)->get();
        $document = $documents[0];

        $templates = Template::where('uid', '=', $document->template)->take(1)->get();
        $template = $templates[0];
        
        $_render = file_get_contents( route( 'documents.template.preview', [$document->uid] ));

        // Replace rules
        $render = $template->layout;
        $render = str_replace([
                '[document.name]',
                '[document.description]',
                '[document.date]',
                '[document.folio]',
                '[person.name]',
                '[person.rut]',
                '[person.email]',
                '[person.anexo1]',
                '[person.anexo2]',
                '[person.anexo3]',
                '[uid]',
            ], [
                $document->name,
                $document->description,
                date('d/F/Y'),
                $document->uid,
                $certificate->nombre,
                $certificate->rut,
                $certificate->email,
                $certificate->anexo1,
                $certificate->anexo2,
                $certificate->anexo3,
                $certificate->uid,
            ], $_render);
        // END Rules

        return PDF::loadHTML($render)
                    //->setPaper('A4')
                    ->setOrientation('landscape')
                    //->setOption('disable-smart-shrinking', true)
                    ->setOption('lowquality', false)
                    ->setOption('zoom', 2)
                    ->setOption('page-height', 297)
                    ->setOption('page-width', 210)
                    ->setOption('margin-top', 0)
                    ->setOption('margin-bottom', 0)
                    ->setOption('margin-left', 0)
                    ->setOption('margin-right', 0)
                    ->inline();

        /*
        $dompdf = new Dompdf();
        $dompdf->set_option('isRemoteEnabled', 'true');

        $dompdf->loadHtml($render);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream($certificate->rut . 'pdf');
        */
    }

    public function edit( $uid ){
        $certificate = Certificate::where('uid', '=', $uid)->take(1)->get();
        $document = $certificate[0]->document;
        $data = array(
            'title' => '',
            'section' => '',
            'certificate' => $certificate,
            'document' => $document,
        );

        return view('certificates.certificate-edit', $data);
    }

    public function update( Request $request, $uid ){
        $certificate = Certificate::where('uid', '=', $uid)->take(1)->get();
        $certificate = Certificate::find($certificate[0]->id);
        $certificate->nombre = $request->input('nombre');
        $certificate->email = $request->input('email');
        $certificate->rut = $request->input('rut');
        $certificate->telefono = $request->input('telefono');
        $certificate->save();

        return redirect()->route('certificates.edit', $uid);
    }

    public function delete( Request $request, $uid ){
        $certificate = Certificate::where('uid', '=', $uid)->take(1)->get();
        $document = $certificate[0]->document;
        $certificate = Certificate::find($certificate[0]->id);
        $certificate->delete();

        return redirect()->route('documents.view', $document);
    }

    public function massiveDelete( Request $request ){
        foreach( $request->input('certificates') as $cert ){
            $certificate = Certificate::find($cert);
            $document = $certificate->document;
            $certificate->delete();
        }
        return redirect()->route('documents.view', $document);
    }

}