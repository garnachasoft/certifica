<?php

namespace App\Http\Controllers;

use App\Document;
use App\Template;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TemplateController extends Controller {

    // NPI: Supongo que esto te permite acceder cuando tengas login
    /*
    public function __construct() {
        $this->middleware('auth');
    }
    /**/

    // Views
    public function viewEditor ($uid) {
        $documents = Document::where('uid', '=', $uid)->take(1)->get();
        $document = $documents[0];


        if ($document->template != '') {
            $templates = Template::where('uid', '=', $document->template)->take(1)->get();
            $template = $templates[0];
        } else {
            $template = new Template;
            $template->layout = '';
        }

        $data = array (
            'title' => 'Editar plantilla',
            'sidebar_mini' => true,
            'section' => 'documents',
            'document' => $document,
            'template' => $template,
        );
        return view('templates.editor', $data);
    }

    public function viewPreview ($uid) {
        $documents = Document::where('uid', '=', $uid)->take(1)->get();
        $document = $documents[0];


        if ($document->template != '') {
            $templates = Template::where('uid', '=', $document->template)->take(1)->get();
            $template = $templates[0];
        } else {
            $template = new Template;
            $template->layout = '';
        }

        $data = array (
            'title' => 'Editar plantilla',
            'sidebar_mini' => true,
            'section' => 'documents',
            'document' => $document,
            'template' => $template,
            'uid' => '$uid',
        );
        return view('templates.preview', $data);
    }

    public function viewPrintable ($uid) {
        $documents = Document::where('uid', '=', $uid)->take(1)->get();
        $document = $documents[0];
        $data = array (
            'title' => 'Editar plantilla',
            'document' => $document,
            'section' => 'documents',
        );
        return view('templates.edit', $data);
    }

    // Actions
    public function actionCreate (Request $request) {
        $template = new Template;
        $template->uid = uniqid();
        $template->layout =  $request->input('content');
        $template->save();

        $documents = Document::where('uid', '=', $request->input('document'))->take(1)->get();
        $document = $documents[0];
        $document->template = $template->uid;
        $document->save();

        return response()->json([
            'status' => '200',
            'template' => $template
        ]);
    }

    public function actionUpdate (Request $request, $uid) {
        $templates = Template::where('uid', '=', $uid)->take(1)->get();
        $template = $templates[0];

        $template->layout =  $request->input('content');
        $template->save();

        return response()->json([
            'status' => '200',
            'template' => $template
        ]);
    }

    public function actionDelete ($uid) {
        $templates = Document::where('uid', '=', $uid)->take(1)->get();
        $template = $templates[0];
        $template->delete();
        return response()->json([
            'status' => '200'
        ]);
    }
    
    // Api
}