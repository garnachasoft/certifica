<?php

namespace App\Http\Controllers;

use App\Sign;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SignController extends Controller {

    // NPI: Supongo que esto te permite acceder cuando tengas login
    public function __construct() {
        $this->middleware('auth');
    }

    // Views
    public function viewIndex () {
        $signs = Sign::all();
        $data = array (
            'title' => 'Firmas',
            'section' => 'signs',
            'signs' => $signs
        );
        return view('signs.index', $data);
    }
    public function viewNew () {
        $data = array (
            'title' => 'Nueva firma',
            'section' => 'signs'
        );
        return view('signs.new', $data);
    }
    public function viewEdit ($uid) {
        $signs = Sign::where('uid', '=', $uid)->take(1)->get();
        $data = array (
            'title' => 'Editar firma',
            'section' => 'signs',
            'sign' => $signs[0]
        );
        return view('signs.edit', $data);
    }

    // Actions
    public function actionCreate (Request $request) {
        $rules = array(
            'name' => 'required',
            'title' => 'required',
            'pic_sign' => 'required',
        );

        $messages = array (
            'name.required' => 'El nombre es necesario',
            'title.required' => 'El título es necesario',
            'pic_sign.required' => 'La firma es obligatoria'
        );

        //check validation
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $sign = new Sign;
            $sign->uid =  uniqid();
            $sign->name = $request->input('name');
            $sign->title = $request->input('title');

            $pic_sign = $request->input('pic_sign');
            $pic_sign = !empty($pic_sign) ? $pic_sign : 'avatar';
            $sign->handwrite = $pic_sign;

            $sign->save();

            return redirect()->route('signs.index');
        }
    }
    public function actionUpdate (Request $request, $uid) {
        $rules = array(
            'name' => 'required',
            'title' => 'required',
            'pic_sign' => 'required',
        );

        $messages = array (
            'name.required' => 'El nombre es necesario',
            'title.required' => 'El título es necesario',
            'pic_sign.required' => 'La firma es obligatoria'
        );

        //check validation
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->route('signs.create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $signs = Sign::where('uid', '=', $uid)->take(1)->get();
            $sign = $signs[0];

            $sign->name = $request->input('name');
            $sign->title = $request->input('title');

            $pic_sign = $request->input('pic_sign');
            $pic_sign = !empty($pic_sign) ? $pic_sign : 'avatar';
            $sign->handwrite = $pic_sign;

            $sign->save();

            return redirect()->route('signs.edit', ['uid', $sign->uid]);
        }
    }
    public function actionDelete ($uid) {
        $signs = Sign::where('uid', '=', $uid)->take(1)->get();
        $sign = $signs[0];

        $sign->delete();
        return redirect()->route('signs.index');
    }
    
    // Api
}