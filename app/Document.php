<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $table = 'documents';

    public function certificates () {
        return $this->hasMany('App\Certificate', 'document', 'uid');
    }
    public function osigns () {
    	$signs = $this->signs;
        return Sign::where('uid', 'REGEXP', "({$signs})")->get();
    }
    public function template () {
        return $this->hasOne('App\Template', 'uid', 'template');
    }
}
