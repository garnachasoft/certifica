
// Utils
var utils = {
	rand: function (a,b) { return Math.floor((Math.random() * b) + a); },
	round: function (n) { return Math.round( n * 100 ) / 100 },
	isOfPercent: function (entero, porcentaje) { return (entero / 100) * porcentaje; },
	isPrecentOf: function (entero, extraido) { return (extraido / entero) * 100; },
	is100pOf: function (entero, porcentaje) { return (entero * 100) / porcentaje; },
	proporcional: function (lado1, lado2, nuevoLado1) {
		v1 = (lado2 / lado1) * nuevoLado1;
		v1 = this.round(v1);
		return v1;
	},
	time2human: function (current, previous) {
		var msPerMinute = 60 * 1000;
		var msPerHour = msPerMinute * 60;
		var msPerDay = msPerHour * 24;
		var msPerMonth = msPerDay * 30;
		var msPerYear = msPerDay * 365;
		var elapsed = current - previous;

		if (elapsed < 15) { return 'Just now'; }
		else if (elapsed < msPerMinute) { return Math.round(elapsed/1000) + ' seconds ago'; }
		else if (elapsed < msPerHour) { return Math.round(elapsed/msPerMinute) + ' minutes ago'; }
		else if (elapsed < msPerDay ) { return Math.round(elapsed/msPerHour ) + ' hours ago'; }
		else if (elapsed < msPerMonth) { return Math.round(elapsed/msPerDay) + ' days ago'; }
		else if (elapsed < msPerYear) { return Math.round(elapsed/msPerMonth) + ' months ago'; }
		else { return Math.round(elapsed/msPerYear ) + ' years ago'; }
	},
	int2month: function (m) { return 'January February March April May June July August September October November December'.split(' ')[m]; },
	int2monthMin: function (m) { return 'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec'.split(' ')[m]; },
	int2day: function (d) { return 'Sunday Monday Tuesday Wednesday Thursday Friday Saturday'.split(' ')[d]; },
	int2dayMin: function (d) { return 'Sun Mon Tue Wed Thu Fri Sat'.split(' ')[d]; },
	date2humanMin: function (datestring) {
		var d = new Date(datestring);
		return utils.int2monthMin(d.getMonth()) + ' ' + d.getDate();
	},
	date2human: function (datestring) {
		var d = new Date(datestring);
		return utils.int2month(d.getMonth()) + ' ' + d.getDate() + ', ' + d.getFullYear();
	},
	date2daytext: function (datestring) {
		var d = new Date(datestring);
		return utils.int2day(d.getDay()) + ' ' + d.getDate() + ', ' + utils.int2month(d.getMonth());
	},
	date2daytextMin: function (datestring) {
		var d = new Date(datestring);
		return utils.int2dayMin(d.getDay()) + ' ' + d.getDate();
	},
	dateFormat: function (d) {
		var format = [
			utils.zeroLeft(d.getMonth() + 1),
			utils.zeroLeft(d.getDate()),
			d.getFullYear()];
		return format.join('-');
	},
	reload: function (route) {
		window.location.reload();
	},
	redirect: function (route) {
		window.location.href = route;
	},
	randHexColor: function () {
		var letters = '89ABCDEF'.split(''), color = '#';
		for (var i = 0; i < 6; i++ ) {
			color += letters[ utils.rand(0, 8) ];
		}
		return color;
	},
	randRGBColor: function () {
		var red = utils.rand(60,140);
		var green = utils.rand(60,140);
		var blue = utils.rand(60,140);
		return 'red,green,blue'
			.replace('red', red)
			.replace('green', green)
			.replace('blue', blue);
	},
	eachDays: function (d1, d2, callback) {
		var _d1 = new Date(d1);
		var _d2 = new Date(d2);
		var auxD = _d1;
		while (auxD < _d2) {
			var resD = auxD;
			callback(resD);
			auxD.setDate( auxD.getDate() + 1 );
		}
	},
	countDays: function (d1, d2) {
		var _d1 = new Date(d1);
		var _d2 = new Date(d2);
		var auxD = _d1;
		var days = 0;
		while (auxD < _d2) {
			auxD.setDate( auxD.getDate() + 1 );
			days++;
		}

		return days;
	},
	imagePicker: function (options) {
		options.start();
	    var reader, picture, file = options.files[0];
	    if (!!file.type.match(/png|jpeg|gif.*/)) {
	        if (!(file.size > options.maxSize)) {
	            if (window.FileReader) {
	                reader = new FileReader();
	                reader.onloadend = function(e) {
	                    picture = e.target.result;
	                    options.process(picture);
	                };
	                reader.readAsDataURL(file);
	            }
	        } else {
	            options.error({
	                code: 2,
	                message: "Max. size is 5 mb"
	            });
	        }
	    } else {
	        options.error({
	            code: 3,
	            message: "Invalid file type"
	        });
	    }
	},
	scroll2: function (sel) {
		var ttop = $(sel).position().top;
        $('html, body').animate({
            scrollTop: ttop
        }, 300);
	},
	isset: function (thevar) {
		return typeof( thevar ) && thevar != '';
	},
	guid: function () {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + s4() + s4();
	},
	pad: function (str, max) {
		var num = new String (str);
		var numstr = num.toString()
		return numstr.length < max ? pad("0" + numstr, max) : numstr;
	}
};